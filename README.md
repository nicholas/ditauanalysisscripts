# DiTauAnalysisScripts

<p align="center">
  <img width="1200" height="800" src="images/eventview.png">
</p>

author: Nicholas Luongo
contact: nicholas.andrew.luongo@cern.ch


Introduction
=====

A set of bash scripts, python scripts, and notebooks intended to be used alongside the DiTauAnalysis athena package found here: https://gitlab.cern.ch/nicholas/ditauanalysis
The bash scripts will allow you to drive the athena package to create ntuple files from AOD or DAOD files. The python scripts and notebooks are generally for analysis of ntuples, though some are for testing AOD files and for driving grid job submission.




Setup
=====


```
git clone ssh://git@gitlab.cern.ch:7999/nicholas/ditauanalysisscripts.git
cd DiTauAnalysis
mkdir samples
```
If you want to set up the DiTauAnalysis athena package then follow its instructions and you will have an "athena" directory with the relevant code.


Run DiTauAnalysis
=====
To e.g. run over the bbtautau 1 TeV signal sample (see DiTauAnalysis README note about file paths):

From base directory
```
cd run
source daod/run_X1000.sh
mv 450522_X1000_bbtautau_lephad.DAOD_HIGGBOOSTEDLH.V0.ntuple.root ../samples
```
Your exact file name above may vary. 

Ntuple Analysis
=====

The files can be analyzed in other python scripts and notebooks by importing from analysis\_samples.py, which will likely also require modification. They can also be accessed as normal ntuples.

Some of the information included in the ntuple:
1. Particles selected by the bbtautau lephad algorithm in both the eHad and muHad channels
2. Skimmed truth particle record
3. Kinematic and quality information for leptons and large-R jets
4. H->bb discriminant values for large-R jets

Some of the main scripts and their functions:
1. bbtautau_helpers.py - Contains commonly used class definitions (Sample, BranchCut) and utility functions for plotting and exploration
2. nn_helpers.py - Class definitions and functions related to neural network training using pytorch
3. truthStudies.ipynb - Exploration of object selections, truth-matching efficiencies, and event topologies
4. FlatDeepNN.ipynb - Training of neural network and performance evaluation
5. AnalysisSelection.ipynb - Development of final event selection for bbtautau lephad analysis


NN Training
=====

Training of the network requires ntuples to be converted from a per event to per system format. This is done with the flatten\_ntuples.py script. Flattened files are then imported through the flat\_samples.py and the network is trained in FlatDeepNN.ipynb. Trained models are saved and then imported in other files as needed. See AnalysisSelection.ipynb for an example of this.
