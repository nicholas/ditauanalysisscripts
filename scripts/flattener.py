

class Flattener():

  def __init__(self):
    pass

  def from_uproot(branches):
    self.ehad_lepton_pt = branches['HadElChosenElePt']
    self.ehad_lepton_eta = branches['HadElChosenEleEta']
    self.ehad_lepton_phi = branches['HadElChosenElePhi']
    self.ehad_lepton_e = branches['HadElChosenEleE']
