import ROOT
import copy

ROOT.gROOT.SetBatch()

f_path = '../samples/ditau_450524_X2000tohh_bbtautau_lephad.recon.AOD.plots.root'
f = ROOT.TFile(f_path)

# Get histograms from ROOT file
truthmatch_id_hist = f.Get('truthmatch_id')
truthmatched_ditaus_per_event_hist = f.Get('truthmatched_ditaus_per_event')
pdg_truthmatch_id_hist = f.Get('pdg_truthmatch_id')

# Rebin pt and dr histograms

# Create derived histograms

c1 = ROOT.TCanvas('c1')
print_path = '../samples/ditau_450524_X2000tohh_bbtautau_lephad.recon.AOD.plots.pdf'
c1.Print(print_path + '[')

truthmatch_id_hist.Draw('hist')
truthmatch_id_hist.GetXaxis().SetTitle('ID')
truthmatch_id_hist.GetYaxis().SetTitle('Entries')
c1.Print(print_path)

truthmatched_ditaus_per_event_hist.Draw('hist')
truthmatched_ditaus_per_event_hist.GetXaxis().SetTitle('# of ditaus')
truthmatched_ditaus_per_event_hist.GetYaxis().SetTitle('Entries')
c1.Print(print_path)

pdg_truthmatch_id_hist.Draw('hist')
pdg_truthmatch_id_hist.GetXaxis().SetTitle('ID')
pdg_truthmatch_id_hist.GetYaxis().SetTitle('Entries')
c1.Print(print_path)

c1.Print(print_path + ']')
