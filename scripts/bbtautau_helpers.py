import numpy as np
import uproot
import awkward as ak
import awkward0
import matplotlib.pyplot as plt
from copy import deepcopy
import uproot3_methods

JaggedTLorentzVectorArray = awkward0.Methods.mixin(uproot3_methods.classes.TLorentzVector.ArrayMethods, awkward0.JaggedArray)

def nice_print_eff(desc, eff):
  print(f'{desc+":":<25} {eff*100:.2f}%') 

def chosen_boosted_eletau_rundown(branches):
  '''
  Run the ele-tau choose over all events in branches and report efficiencies for all steps

  param branches: Branches to run over
  '''
  tracker = DiTauEfficiencyTracker(branches, 'HadEl')
  for i, event_branches in enumerate(branches):
    choose_boosted_eletau_event(event_branches, tracker)
  tracker.summarize()

def chosen_boosted_eletau_efficiencies(branches):
  '''
  Compare truth ele-tau to chosen and report final efficiencies

  param branches: Branches to run over
  '''
  truth_electrons = create_p4s_branch(branches, 'TruthFinalLepton')
  truth_taus = create_p4s_branch(branches, 'TruthHadTauVis')
  electrons = create_p4s_branch(branches, 'HadElChosenEle')
  taus = create_p4s_branch(branches, 'HadElChosenTau')
  electron_eff = calculate_chosen_truth_efficiencies(truth_electrons, electrons, 0.1)
  tau_eff = calculate_chosen_truth_efficiencies(truth_taus, taus, 0.1)
  return electron_eff, tau_eff

def chosen_boosted_mutau_efficiencies(branches):
  '''
  Compare truth mu-tau to chosen and report final efficiencies

  param branches: Branches to run over
  '''
  truth_muons = create_p4s_branch(branches, 'TruthFinalLepton')
  truth_taus = create_p4s_branch(branches, 'TruthHadTauVis')
  muons = create_p4s_branch(branches, 'HadMuChosenMu')
  taus = create_p4s_branch(branches, 'HadMuChosenTau')
  muon_eff = calculate_chosen_truth_efficiencies(truth_muons, muons, 0.1)
  tau_eff = calculate_chosen_truth_efficiencies(truth_taus, taus, 0.1)
  return muon_eff, tau_eff

def choose_boosted_eletau_branches(branches):
  '''
  Create lists of the chosen electrons and taus for all events in branches

  param branches: Set of events to run over
  '''
  electrons = [0] * len(branches)
  taus = [0] * len(branches)
  for i, event_branches in enumerate(branches):
    electron, tau = choose_boosted_eletau_event(event_branches)
    electrons[i] = electron
    taus[i] = tau
  electrons = tlorentzvectorarray_from_vectors(electrons)
  taus = tlorentzvectorarray_from_vectors(taus)
  return electrons, taus

def choose_boosted_mutau_branches(branches):
  '''
  Create lists of the chosen muons and taus for all events in branches

  param branches: Set of events to run over
  '''
  muons = [0] * len(branches)
  taus = [0] * len(branches)
  for i, event_branches in enumerate(branches):
    muon, tau = choose_boosted_eletau_event(event_branches)
    electrons[i] = electron
    taus[i] = tau
  electrons = tlorentzvectorarray_from_vectors(electrons)
  taus = tlorentzvectorarray_from_vectors(taus)
  return electrons, taus

def choose_boosted_eletau_event(event_branches, eff_tracker=None):
  '''
  Apply algorithm for finding the correct reconstructed objects corresponding to truth lepton and tau

  param i: Index of event, useful for tracking
  param event_branches: One event slice of branches
  param eff_tracker: DiTauEfficiencyTracker for getting efficiencies at each step
  return chosen_electron: Chosen electron
  return chosen_tau: Chosen tau
  '''
  ditaus = create_p4s_branch(event_branches, 'HadElDiTau')
  ditau_bdt_scores = event_branches['HadElDiTauBDTScore']
  ditau_taus = create_p4s_branch(event_branches, 'HadElTau')
  ditau_electrons = create_p4s_branch(event_branches, 'HadElElectron')
  electrons = create_p4s_branch(event_branches, 'Ele')
  loose_electrons = create_p4s_branch(event_branches, 'EleLoose')
  taus = create_p4s_branch(event_branches, 'Tau')

  chosen_electron = 0
  chosen_tau = 0

  # Step 1: Find leading loose electron
  if len(loose_electrons) > 0:
    loose_electrons_by_pt = [electron for electron in sorted(loose_electrons, key = lambda x: x.pt)] 
    leading_electron = loose_electrons_by_pt[-1]
    chosen_electron = leading_electron

  if eff_tracker:
    eff_tracker.checkpoint('After choosing leading loose electron', chosen_electron, 0) 

  # Step 2: Find tau closest to electron
  if chosen_electron and len(taus) > 0:
    chosen_tau, chosen_tau_dr = nearest_candidate_to_anchor(chosen_electron, taus)

  if eff_tracker:
    eff_tracker.checkpoint('After choosing tau closest to electron', chosen_electron, chosen_tau)

  # Step 3: Look for another tau with 0.1 < dR < 1.0 from electron and pt > 20 GeV if chosen tau is too close (meaning tau and electron are overlapping)
  if chosen_electron and chosen_tau and chosen_tau_dr < 0.1:
    # Get dR, tau pairs within 0.1 < dR < 1.0 and pt > 20 GeV, min dR to prevent self-selection
    restr_dr_taus = [tau for tau in taus if 0.1 < chosen_electron.delta_r(tau) < 1 and tau.pt > 20]
    if len(restr_dr_taus) > 0:
      chosen_tau, _ = nearest_candidate_to_anchor(chosen_tau, restr_dr_taus)

  if eff_tracker:
    eff_tracker.checkpoint('After choosing tau with 0.1 < dR < 1.0 and pt > 20 GeV if available', chosen_electron, chosen_tau)

  # Step 4: If no other suitable tau was found and there is another loose electron within 1.0 switch to that
  if chosen_electron and chosen_tau and chosen_tau_dr < 0.1 and len(restr_dr_taus) == 0:
    # If no other tau found, look for other loose electron within  0.1 < dR < 1.0 (min is to avoid self-selection)
    close_loose_ele = [electron for electron in loose_electrons if 0.1 < chosen_electron.delta_r(electron) < 1]
    if len(close_loose_ele) > 0:
      chosen_electron, _ = nearest_candidate_to_anchor(chosen_electron, close_loose_ele)

  if eff_tracker:
    eff_tracker.checkpoint('After choosing other loose electron within 0.1 < dR < 1.0 if no tau found', chosen_electron, chosen_tau)

  # Get dR of current tau and electron
  if chosen_tau and chosen_electron:
    current_dr = chosen_tau.delta_r(chosen_electron)
  # Step 5: If electron and tau still within 0.2, look for ditau overlapping the current electron
  if chosen_electron and chosen_tau and current_dr < 0.2:
    # Find all indices of ditaus whose electron is within 0.1 of current electron
    close_ditau_electron_indices = [j for j, ditau_electron in enumerate(ditau_electrons) if ditau_electron.delta_r(chosen_electron) < 0.1]
    # Only continue if we found at least one ditau
    if len(close_ditau_electron_indices) > 0:
      close_ditaus = ditaus[close_ditau_electron_indices]
      # Order the indices by ditau pt
      close_ditau_indices_ordered = [index for _,index in sorted(zip(close_ditaus, close_ditau_electron_indices), key=lambda x:x[0].pt)]
      # Choose last in ordering = highest pt
      chosen_ditau_index = close_ditau_indices_ordered[-1]
      # Assign ditau tau from given index (electron should be the same already)
      chosen_tau = ditau_taus[chosen_ditau_index]

  if eff_tracker:
    eff_tracker.checkpoint('After finding ditau with electron overlap if current dR < 0.2', chosen_electron, chosen_tau)

  return chosen_electron, chosen_tau


def create_p4s_branch(branches, branch_name):
  '''
  Return an array (or jagged array) of TLorentzVectors from the separate pt/eta/phi/em branches

  param branches: Set of branches which must contain XPt, XEta, XPhi, and XE/XM branches
  param branch_name: Name of object that must fit into pt/eta/phi/em names above
  '''
  pt_branch_name = '{}Pt'.format(branch_name)
  eta_branch_name = '{}Eta'.format(branch_name)
  phi_branch_name = '{}Phi'.format(branch_name)
  if branch_name in ['TruthHadTauVis', 'HadElDiTau', 'HadMuDiTau']:
    from_branches = uproot3_methods.TLorentzVectorArray.from_ptetaphim
    em_branch_name = '{}M'.format(branch_name)
  else:
    from_branches = uproot3_methods.TLorentzVectorArray.from_ptetaphie
    em_branch_name = '{}E'.format(branch_name)

  try:
    branches[pt_branch_name]
  except:
    raise ValueError('Invalid branch name given')

  try:
    np.asarray(branches[pt_branch_name])
    is_flat = 1
  except:
    is_flat = 0
    counts_branch_name = 'N{}'.format(branch_name)
    if branch_name in ['HadElTau', 'HadElElectron']:
      counts_branch_name = 'NHadElDiTau'
    elif branch_name in ['HadMuTau', 'HadMuMuon']:
      counts_branch_name = 'NHadMuDiTau'

  # Input branches are not jagged (one per event)
  if is_flat:
    pt_branch = branches[pt_branch_name]
    eta_branch = branches[eta_branch_name]
    phi_branch = branches[phi_branch_name]
    em_branch = branches[em_branch_name]
    # May only be a single event in which case must placed into list
    if not isinstance(branches[pt_branch_name], ak.Array):
      pt_branch = [pt_branch] 
      eta_branch = [eta_branch] 
      phi_branch = [phi_branch] 
      em_branch = [em_branch] 
    jagged = from_branches(pt_branch, eta_branch, phi_branch, em_branch)
  # Input branches are jagged
  else:
    pt_branch = ak.flatten(branches[pt_branch_name])
    eta_branch = ak.flatten(branches[eta_branch_name])
    phi_branch = ak.flatten(branches[phi_branch_name])
    em_branch = ak.flatten(branches[em_branch_name])
    counts_branch = branches[counts_branch_name]
    
    flat = from_branches(pt_branch, eta_branch, phi_branch, em_branch)

    jagged = JaggedTLorentzVectorArray.fromcounts(counts_branch, flat)

  return jagged


def nearest_candidate_to_anchor(anchor, candidates):
  '''
  Find the nearest particle in candidates list to anchor particle

  param anchor: Reference particle
  param candidates: List of particles, closest in dR to anchor to be selected
  return nearest_candidate: The particle in candidates closest in dR to anchor
  return nearest_dr: The dR of the nearest particle
  '''
  candidate_drs = [anchor.delta_r(candidate) for candidate in candidates]
  candidates_dr_sorted = [candidate for dr, candidate in sorted(zip(candidate_drs, candidates))]
  nearest_candidate = candidates_dr_sorted[0]
  nearest_dr = min(candidate_drs)
  return nearest_candidate, nearest_dr


def reco_nearest_dr_match(all_truth_vecs, all_reco_vecs):
  '''
  Find the nearest reconstructed object in dR to each truth object

  param all_truth_vecs: A list of TLorentzVectors, one truth vector for each event
  param all_reco_vecs: A list of lists of TLorentzVectors, one list of reco vectors for each event
  return: A TLorentzVectorArray of the nearest reco vector to the truth vector in dR
  '''
 
  reco_match_pts = []
  reco_match_etas = []
  reco_match_phis = []
  reco_match_es = []
  for truth_vec, reco_vecs in zip(all_truth_vecs, all_reco_vecs):
    reco_truth_drs = reco_vecs.delta_r(truth_vec)
    if len(reco_truth_drs) == 0:
      reco_match_pts.append(0)
      reco_match_etas.append(0)
      reco_match_phis.append(0)
      reco_match_es.append(0)
      continue
    min_dr = min(reco_truth_drs)
    min_i = np.where(reco_truth_drs == min_dr)[0][0]
    min_vec = reco_vecs[min_i]
    reco_match_pts.append(min_vec.pt)
    reco_match_etas.append(min_vec.eta)
    reco_match_phis.append(min_vec.phi)
    reco_match_es.append(min_vec.energy)
  reco_matches = uproot3_methods.TLorentzVectorArray.from_ptetaphim(reco_match_pts, reco_match_etas, reco_match_phis, reco_match_es)
  return reco_matches


def pad_eff_hist_ranges(y_vals, x_low_range, x_high_range, x_bin_step):
  '''
  Pad lists used for efficiency plots to play nice with plt steps format

  param y_vals: List of y values to plot
  param x_low_range: Minimum x value in plot
  param x_high_range: Maximum x value in plot
  param x_bin_step: Size of x bin
  return plot_x: List of x values to feed into plot
  return plot_y: List of y values to feed into plot
  '''
  plot_x = [x_low_range] + list(np.arange(x_low_range+x_bin_step, x_high_range+x_bin_step, x_bin_step))
  plot_y = np.concatenate((np.array([y_vals[0]]), y_vals))
  return plot_x, plot_y


def create_eff_histograms(num_values, denom_values, low_range, high_range, bin_num):
  '''
  Create histograms that are divided to get an efficiency plot

  param num_values: Histogram of numerator values
  param denom_values: Histogram of denominator values
  param low_range: Minimum value for histogram
  param high_range: Maximum value for histogram
  param bin_num: Number of bins in histogram
  return eff_hist: Histogram holding efficiency in each bin
  '''

  num_hist = np.histogram(num_values, bin_num, (low_range, high_range))[0]
  denom_hist = np.histogram(denom_values, bin_num, (low_range, high_range))[0]
  eff_hist = np.divide(num_hist, denom_hist)

  return eff_hist


def eff_hist_lists(num_values, denom_values, low_range, high_range, bin_num):
  '''
  Create lists for axes of efficiency histograms

  param num_values: Histogram of numerator values
  param denom_values: Histogram of denominator values
  param low_range: Minimum value for histogram
  param high_range: Maximum value for histogram
  param bin_num: Number of bins in historam

  return plot_x: List of x values to feed into plot
  return plot_y: List of y values to feed into plot
  '''

  bin_step = high_range / bin_num
  eff_hist = create_eff_histograms(num_values, denom_values, low_range, high_range, bin_num)
  # This silliness is so that the first bin actually gets plotted
  plot_x, plot_y = pad_eff_hist_ranges(eff_hist, low_range, high_range, bin_step)

  return plot_x, plot_y


def ntuple_branches_from_path(file_path):
  '''
  Read the file at the given path, access the ntuple, and return the formatted branches with uproot

  param file_path: Path to file containing ntuple

  return branches: Array of branches of ntuple
  '''
  f = uproot.open(file_path)
  t = f['mytree']
  branches = t.arrays()
  return branches


def fiducial_region_cut(branches):
  '''
  Filter events to only those in defined fiducial region

  param branches: Branches to be filtered 

  return out_branches: Filtered branches
  '''
  true_hadtau_pt_fiducial_mask = branches['TruthHadTauVisPt'] > 20
  true_electron_pt_fiducial_mask = branches['TruthFinalLeptonPt'] > 20
  true_ditau_vis_pt_fiducial_mask = branches['TruthDiTauVisPt'] > 300
  true_had_tau_vis_eta_fiducial_mask = abs(branches['TruthHadTauEta']) < 2.5
  true_electron_eta_fiducial_mask = abs(branches['TruthFinalLeptonEta']) < 2.5

  all_fiducial_masks = [true_hadtau_pt_fiducial_mask, true_electron_pt_fiducial_mask, true_ditau_vis_pt_fiducial_mask, true_had_tau_vis_eta_fiducial_mask, true_electron_eta_fiducial_mask]
  fiducial_mask = ak.all(all_fiducial_masks, 0)

  out_branches = branches[fiducial_mask]
  return out_branches


def hist_norm_weights(vals, bin_width):
  '''
  Create a list of weights to be used for normalizing a histogram

  param vals: The values used to create the histogram
  param bin_width: The bin width of the histogram

  return weight: List of weights
  '''
  weights = np.ones_like(vals) / float(len(vals))
  weights = weights / bin_width
  return weights


def create_weighted_hist(vals, bins, min_range, max_range, weight, label):
  '''
  Create a histogram with all events weighted by the given weight value

  param vals: The values to fill the histogram
  param bins: The number of bins
  param min_range: Minimum value
  param max_range: Maximum value
  param weight: Value to weight entries
  param label: Legend label

  return bin_vals: A list of the values of each histogram bin
  '''
  weights = np.ones_like(vals) * float(weight)
  bin_vals = plt.hist(vals, bins=bins, range=(min_range,max_range), weights=weights, label=label, alpha=0.5)
  return bin_vals


def norm_hist_weights(vals, bins, min_range, max_range):
  '''
  Get weights to apply to a histogram in order to normalize it

  param vals: The values to fill the histogram
  param bins: The number of bins
  param min_range: Minimum value
  param max_range: Maximum value

  return weights: Weights applied to each bin
  '''
  bin_width = (max_range - min_range) / bins
  weights = np.ones_like(vals) / float(len(vals) * bin_width)
  return weights


def create_normed_hist(vals, bins, min_range, max_range, label):
  '''
  Create a normalized histogram from given values and properties

  param vals: The values to fill the histogram
  param bins: The number of bins
  param min_range: Minimum value
  param max_range: Maximum value
  param label: Legend label

  return bin_weights: Weights applied to each bin
  '''
  weights = norm_hist_weights(vals, bins, min_range, max_range) 
  bin_weights = plt.hist(vals, bins=bins, range=(min_range,max_range), weights=weights, label=label, alpha=0.5)
  return bin_weights


def normed_hist_from_branches(branches, branch_name, bins, min_range, max_range, label, selection=None):
  '''
  Create a normalized histogram from the given branch, applying a selection beforehand if supplied

  param branches: Sample of branches containing the branch to be plotted
  param branch_name: Name of branch to be plotted
  param bins: The number of bins
  param min_range: Minimum value
  param max_range: Maximum value
  param label: Legend label
  param selection: Selection to be applied to branches before plotting

  return norm_weights: Weights applied to normalize histogram
  '''
  if selection:
    cut_branches = selection.apply_cuts_to_branches(branches)
    norm_weights = create_normed_hist(cut_branches[branch_name], bins, min_range, max_range, label)
  else:
    norm_weights = create_normed_hist(branches[branch_name], bins, min_range, max_range, label)

  return norm_weights
  

def plot_before_after_selection(branches, branch_name, selection, bins, min_range, max_range, label, before_selection=None):
  '''
  Plot a histogram of the given branch before and certain selections are applied

  param branches: Sample of branches containing the branch to be plotted
  param branch_name: Name of branch to be plotted
  param selection: Selection to be applied for "after"
  param bins: The number of bins
  param min_range: Minimum value
  param max_range: Maximum value
  param label: Legend label
  param before_selection: Selection to already be applied for "before". Note should be a subset of the after selection!
  '''
  if before_selection:
    normed_hist_from_branches(branches, branch_name, bins, min_range, max_range, label='{} (Before)'.format(label), selection=before_selection)
  else:
    normed_hist_from_branches(branches, branch_name, bins, min_range, max_range, label='{} (Before)'.format(label))

  normed_hist_from_branches(branches, branch_name, bins, min_range, max_range, label='{} (After)'.format(label), selection=selection)

  
def get_leading_indices(pt_branch):
  '''
  Create a list of the index of the highest pt object for the supplied branch

  param pt_branch: Awkward array of pts for objects in an event

  return leading_inds: A list with the index of the highest pt object for each event
  '''
  max_pts = [max(pts) if len(pts) > 0 else -1 for pts in pt_branch]
  leading_inds = np.array([np.where(pts == max_pt)[0][0] if max_pt != -1 else -1 for pts, max_pt in zip(pt_branch,max_pts)]) 
  return leading_inds


def fill_leading_ptetaphie_variables(branches, pt_name, eta_name, phi_name, e_name):
  '''
  Create new branches containing information about the leading object in each event

  param branch: Set of branches returned by uproot
  param pt_name: Name of the branch holding pt information
  param eta_name: Name of the branch holding eta information
  param phi_name: Name of the branch holding phi information
  param e_name: Name of the branch holding energy information
  '''
  pt_branch = branches[pt_name]
  eta_branch = branches[eta_name]
  phi_branch = branches[phi_name]
  e_branch = branches[e_name]
  leading_indices = get_leading_indices(pt_branch)
  branches['Leading{}'.format(pt_name)] = [pts[index] if index != -1 else -1 for pts, index in zip(pt_branch, leading_indices)]
  branches['Leading{}'.format(eta_name)] = [etas[index] if index != -1 else -1 for etas, index in zip(eta_branch, leading_indices)]
  branches['Leading{}'.format(phi_name)] = [phis[index] if index != -1 else -1 for phis, index in zip(phi_branch, leading_indices)]
  branches['Leading{}'.format(e_name)] = [es[index] if index != -1 else -1 for es, index in zip(e_branch, leading_indices)]


def fill_all_leading_objects(branches):
  '''
  Fill new branches with Pt/Eta/Phi/E information for all objects of interest

  param branches: Set of branches to be modified
  '''
  fill_leading_ptetaphie_variables(branches, 'JetPt', 'JetEta', 'JetPhi', 'JetE')
  fill_leading_ptetaphie_variables(branches, 'LargeRJetPt', 'LargeRJetEta', 'LargeRJetPhi', 'LargeRJetE')
  fill_leading_ptetaphie_variables(branches, 'TauPt', 'TauEta', 'TauPhi', 'TauE')
  fill_leading_ptetaphie_variables(branches, 'TauLoosePt', 'TauLooseEta', 'TauLoosePhi', 'TauLooseE')
  fill_leading_ptetaphie_variables(branches, 'MuPt', 'MuEta', 'MuPhi', 'MuE')
  fill_leading_ptetaphie_variables(branches, 'MuLoosePt', 'MuLooseEta', 'MuLoosePhi', 'MuLooseE')
  fill_leading_ptetaphie_variables(branches, 'ElePt', 'EleEta', 'ElePhi', 'EleE')
  fill_leading_ptetaphie_variables(branches, 'EleLoosePt', 'EleLooseEta', 'EleLoosePhi', 'EleLooseE')

def create_variable_id_branch(branches, object_name, id_name, variable_name):
  '''
  Create a new filtered branch from an existing object branch (ele, tau) and an available ID WP (loose, tight). A branch of the object variable must and a branch flagging wihch objects pass the WP.

  param branches: Set of branches to be added to
  param object_name: Name of physics object (Ele, Tau) as it appears in existing branches
  param id_name: Name of ID WP (Loose, Tight) as it appears in existing branches
  param variable_name: Name of the variable (Pt, Eta) as it appears in the branch to be filtered
  '''
  current_branch_name = '{}{}'.format(object_name, variable_name)
  id_branch_name = '{}{}{}'.format(object_name, id_name, variable_name)
  mask_branch_name = '{}{}'.format(object_name, id_name)

  current_branch = branches[current_branch_name]
  mask_branch = branches[mask_branch_name]
  branches[id_branch_name] = [vars[masks == 1] for vars, masks in zip(current_branch, mask_branch)]

def create_object_id_branches(branches, object_name, id_name):
  '''
  Create new standard pt/eta/phi/e branches for the given object and ID WP

  param branches: Set of branches to be added to
  param object_name: Name of physics object (Ele, Tau) as it appears in existing branches
  param variable_name: Name of ID WP (Loose, Tight) as it appears in existing branches
  '''
  create_variable_id_branch(branches, object_name, id_name, 'Pt')
  create_variable_id_branch(branches, object_name, id_name, 'Eta')
  create_variable_id_branch(branches, object_name, id_name, 'Phi')
  create_variable_id_branch(branches, object_name, id_name, 'E')

def tlorentzvectorarray_from_vectors(vectors):
  '''
  Create a TLorentzVectorArray from a simple iterable of TLorentzVectors (this must already exist somewhere). Note currently only works with 1-D iterable

  param vectors: Iterable of TLorentzVectors
  '''
  pts = [vector.pt if vector != 0 else 0 for vector in vectors]
  etas = [vector.eta if vector != 0 else 0 for vector in vectors]
  phis = [vector.phi if vector != 0 else 0 for vector in vectors]
  es = [vector.E if vector != 0 else 0 for vector in vectors]
  new_array = uproot3_methods.TLorentzVectorArray.from_ptetaphie(pts, etas, phis, es)
  return new_array

def calculate_chosen_truth_efficiencies(truth_p4s, chosen_p4s, max_dr):
  passed = 0
  for truth_p4, chosen_p4 in zip(truth_p4s, chosen_p4s):
    if chosen_p4 == 0 or chosen_p4.pt == 0:
      continue
    else:
      dr = truth_p4.delta_r(chosen_p4)
      if dr < max_dr:
        passed += 1
  return (passed / len(truth_p4s))

def efficiency_plot(samples, branch_name, min_range, max_range, bin_num, min_max_eq='min'):
  '''
  Produce just the efficiency plot of the eff_sens_plot ensemble

  param branch_name: Name of the branch to be summarized
  param min_range: Minimum value of the variable
  param max_range: Maximum value of the variable
  param bin_num: Number of bins/steps of the variable
  param min_max_eq: Comparison method for cuts, string min, max, or eq
  '''

  step = float(max_range - min_range) / bin_num
  pt_wps = np.array(np.arange(min_range, max_range, step))

  for sample in samples:
    
    selection = Selection()
    
    # Efficiency calculations use fiducial events for signal
    if sample.is_signal:
      eff_branches = sample.fid_branches
    else:
      eff_branches = sample.branches

    eff_branches = selection.apply_cuts_to_branches(eff_branches)
    effs = np.array([])

    for i in pt_wps:
      selection.set_cut(branch_name, i, min_max_eq)
      
      passed_eff_events = selection.nevents_passing_cuts(eff_branches)
      eff = passed_eff_events / len(eff_branches)
      effs = np.append(effs, eff)

    label = sample.short_name
    if sample.is_signal:
      label += ' (fiducial)'
    plt.plot(pt_wps, effs, label=label, linewidth=3)

  plot_var = ''
  if 'Pt' in branch_name:
    plot_var = 'Pt'
  elif 'BDT' in branch_name:
    plot_var = 'BDT Score'
  elif 'M' in branch_name:
    plot_var = 'Mass'
  elif 'dR' in branch_name:
    plot_var = 'dR'
  elif 'dPhi' in branch_name:
    plot_var = 'dPhi'
  elif 'dEta' in branch_name:
    plot_var = 'dEta'
  elif 'NN' in branch_name:
    plot_var = 'Output'
  elif 'Disc' in branch_name:
    plot_var = 'Discriminant'
  else:
    plot_var = 'Value'

  plt.axhline(y=0.9, xmin=0, xmax=1, linewidth=1, color='black', linestyle='--')
  plt.axvline(x=350, ymin=0, ymax=1, linewidth=1, color='black')
  plt.title('Efficiency vs Leading Large-R Jet Pt'.format(plot_var), size=20)
  plt.xlabel(plot_var, size=20, labelpad=0)
  plt.xticks(fontsize=15)
  plt.ylabel('Efficiency', size=20)
  plt.yticks(fontsize=15)
  plt.xlim(min_range, max_range)
  plt.legend(fontsize=16)

  plt.show()   
      

def eff_sens_plots(samples, branch_name, min_range, max_range, bin_num, channel, input_selection=None, cutoff_eff=0, log_eff_yn=0, log_hist_yn=0, min_max_eq='min', flat_effs={}):
  '''
  Produce a set of plots comparing a given branch across many samples. The plots are histogram, efficiency, yields, and sensitivity

  param branch_name: Name of the branch to be summarized
  param min_range: Minimum value of the variable
  param max_range: Maximum value of the variable
  param bin_num: Number of bins/steps of the variable
  param channel: Decay channel, All, HadEl, or HadMu
  param input_selection: Selection class instance holding a preliminary selection to be applied before cuts to the variable
  param cutoff_eff: Reference efficiency
  param log_eff_yn: Flag to plot the efficiency with a log y-axis
  param log_hist_yn: Flag to draw histograms with a log y-axis
  param min_max_eq: Comparison method for cuts, string min, max, or eq
  param flat_effs: A dictionary of overall efficiencies to be applied to each sample. Key is sample object itself
  '''
  import math

  plt.rcParams['figure.figsize'] = [15, 8]
  plt.rcParams['figure.dpi'] = 150
  
  step = float(max_range - min_range) / bin_num
  pt_wps = np.array(np.arange(min_range, max_range, step))
    
  figs, axs = plt.subplots(2, 2)
     
  for sample in samples:
    if input_selection:
      selection = input_selection.make_copy()
    else:
      selection = Selection()

    cuts = selection.cuts
    if branch_name in cuts.keys():
      existing_cut_val = cuts[branch_name].cut_value
      selection.remove_cut(branch_name)
    else:
      existing_cut_val = None
      
    flat_eff = 0
    if flat_effs != {}: 
      flat_eff = flat_effs[sample]

    # Efficiency calculations use fiducial events for signal
    if sample.is_signal:
      if channel == 'All':
        eff_branches = sample.fid_branches
        branches = sample.branches
      elif channel == 'HadEl':
        eff_branches = sample.fid_hadel_branches
        branches = sample.hadel_branches
      elif channel == 'HadMu':
        eff_branches = sample.fid_hadmu_branches
        branches = sample.hadmu_branches
      else:
        print('Invalid channel')
        break
      sens = np.array([])
    else:
      eff_branches = sample.branches
      branches = sample.branches

    eff_branches = selection.apply_cuts_to_branches(eff_branches)
    branches = selection.apply_cuts_to_branches(branches)
    
    effs = np.array([])
    weighted_events = np.array([])
    for i in pt_wps:
      selection.set_cut(branch_name, i, min_max_eq)
      
      cut_eff_branches = selection.apply_cuts_to_branches(eff_branches)
      cut_branches = selection.apply_cuts_to_branches(branches)
      passed_eff_events = sum(cut_eff_branches['OverallWeight'])
      passed_events = sum(cut_branches['OverallWeight'])
      nevents = sum(eff_branches['OverallWeight'])

      if not nevents:
        nevents = 1
      eff = passed_eff_events / nevents

      if flat_eff:
        eff *= flat_eff
        passed_events *= flat_eff

      effs = np.append(effs, eff)
      
      weighted_events = np.append(weighted_events, passed_events)
      
      # Only calculate sensitivity for signal
      if sample.is_signal:
        weighted_background_events = 0
        # Loop over only background samples for total background events
        for background_sample in samples:
          if background_sample.is_signal:
            continue
          background_branches = selection.apply_cuts_to_branches(background_sample.branches)
          background_sample_weighted_events = sum(background_branches['OverallWeight'])

          if flat_eff:
            background_sample_weighted_events *= flat_effs[background_sample]

          weighted_background_events += background_sample_weighted_events
          
        if not weighted_background_events:
          weighted_background_events = 1
        sen = passed_events / math.sqrt(weighted_background_events)
        sens = np.append(sens, sen)
        
    axs[0,0].hist(eff_branches[branch_name], bins=bin_num, range=(min_range,max_range), label=sample.short_name, weights=eff_branches['OverallWeight'], alpha=0.5, density=True)
    axs[0,1].plot(pt_wps, effs, label=sample.short_name)
    axs[1,0].plot(pt_wps, weighted_events, label=sample.short_name)
    if sample.is_signal:
      axs[1,1].plot(pt_wps, sens, label=sample.short_name)
      
  plot_var = ''
  if 'Pt' in branch_name:
    plot_var = 'Pt'
  elif 'BDT' in branch_name:
    plot_var = 'BDT Score'
  elif 'M' in branch_name:
    plot_var = 'Mass'
  elif 'dR' in branch_name:
    plot_var = 'dR'
  elif 'dPhi' in branch_name:
    plot_var = 'dPhi'
  elif 'dEta' in branch_name:
    plot_var = 'dEta'
  elif 'NN' in branch_name:
    plot_var = 'KNN Output'
  elif 'Disc' in branch_name:
    plot_var = 'Discriminant'
  else:
    plot_var = 'Value'
      
  if existing_cut_val is not None:
    axs[0,0].axvline(x=existing_cut_val, color='black', ls='--')
  axs[0,0].set_title('{} (Normalized)'.format(plot_var))
  axs[0,0].set_xlabel(plot_var, size=10, labelpad=0)
  axs[0,0].set_ylabel('A.U.', size=10)
  if log_hist_yn:
    axs[0, 0].set_yscale('log')
  axs[0,0].set_xlim(min_range, max_range)
  axs[0,0].legend(fontsize=10)
  
  if existing_cut_val is not None:
    axs[0,1].axvline(x=existing_cut_val, color='black', ls='--')
  axs[0,1].axhline(y=cutoff_eff, xmin=0, xmax=1, linewidth=1, color='black')
  axs[0,1].set_title('Efficiency vs {}'.format(plot_var), size=10)
  axs[0,1].set_xlabel(plot_var, size=10, labelpad=0)
  axs[0,1].set_ylabel('Efficiency', size=10)
  if log_eff_yn:
    axs[0,1].set_yscale('log')
  axs[0,1].set_xlim(min_range, max_range)
  axs[0,1].legend(fontsize=10)
  
  if existing_cut_val is not None:
    axs[1,0].axvline(x=existing_cut_val, color='black', ls='--')
  #axs[1,0].axvline(x=cutoff_wp_16tev, ymin=0, ymax=1, linewidth=1, color='black')
  #axs[1,0].axvline(x=cutoff_wp_20tev, ymin=0, ymax=1, linewidth=1, color='black')
  axs[1,0].set_title('Yields vs {}'.format(plot_var), size=10)
  axs[1,0].set_xlabel(plot_var, size=10, labelpad=0)
  axs[1,0].set_ylabel('Yield', size=10)
  axs[1,0].set_yscale('log')
  axs[1,0].set_xlim(min_range, max_range)
  axs[1,0].legend(fontsize=10)
  
  if existing_cut_val is not None:
    axs[1,1].axvline(x=existing_cut_val, color='black', ls='--')
  #axs[1,1].axvline(x=cutoff_wp_16tev, ymin=0, ymax=1, linewidth=1, color='black')
  #axs[1,1].axvline(x=cutoff_wp_20tev, ymin=0, ymax=1, linewidth=1, color='black')
  axs[1,1].set_title('Sensitivity vs {}'.format(plot_var), size=10)
  axs[1,1].set_xlabel(plot_var, size=10, labelpad=0)
  axs[1,1].set_ylabel('Sensitivity', size=10)
  axs[1,1].set_xlim((min_range, max_range))
  axs[1,1].legend(fontsize=10)
    
  plt.show()   

def pairwise_corr_efficiencies(samples, x_branch, x_cut, y_branch, y_cut, preselection=None):
  from scipy.stats import pearsonr

  print(f'{x_branch} + {y_branch}')
  if not preselection:
    selection = Selection()
  else:
    selection = preselection.make_copy()
  selection.set_cut(x_branch, x_cut, 'min')
  selection.set_cut(y_branch, y_cut, 'min')
  for sample in samples:
    print(sample.short_name)
    if sample.is_signal:
      branches = sample.fid_branches
    else:
      branches = sample.branches
    nevents_cut = selection.nevents_passing_cuts(branches)
    calculated_eff = nevents_cut / len(branches)
    
    if not preselection:
      x_selection = Selection()
    else:
      x_selection = preselection.make_copy()
    x_selection.set_cut(x_branch, x_cut, 'min')
    x_nevents_cut = x_selection.nevents_passing_cuts(branches)
    x_eff = x_nevents_cut / len(branches)
    
    if not preselection:
      y_selection = Selection()
    else:
      y_selection = preselection.make_copy()
    y_selection.set_cut(y_branch, y_cut, 'min')
    y_nevents_cut = y_selection.nevents_passing_cuts(branches)
    y_eff = y_nevents_cut / len(branches)
    
    estimated_eff = x_eff * y_eff
    
    print(f"{'Observed eff: ':<30}{calculated_eff:.8f}")
    print(f"{'Estimated eff: ':<30}{estimated_eff:.8f}")
    print(f"{'Estimated events: ':<30}{estimated_eff * len(branches)}")
    print(f"{'MC weight: ':<30}{sample.mc_weight}")
    print(f"{'Estimated events (weighted): ':<30}{estimated_eff * sample.mc_weight * len(branches)}")
    correlation, _ = pearsonr(branches[x_branch], branches[y_branch])
    print(f'R: {correlation:.2f}')
    print()  

def corr_plot(branches, x_branch, x_range, x_steps, y_branch, y_range, y_steps):
  x_step_len = float(x_range[1] - x_range[0]) / x_steps
  x_wps = np.arange(x_range[0], x_range[1] + x_step_len, x_step_len)
  x_steps += 1

  y_step_len = float(y_range[1] - y_range[0]) / y_steps
  y_wps = np.arange(y_range[0], y_range[1] + y_step_len, y_step_len)
  y_steps += 1

  selection = Selection()
  effs = np.zeros([y_steps, x_steps])

  nevents = len(branches)
  for i, x_wp in enumerate(x_wps):
    selection.set_cut(x_branch, x_wp, 'min')
    for j, y_wp in enumerate(y_wps):
      selection.set_cut(y_branch, y_wp, 'min')

      nevents_cut = selection.nevents_passing_cuts(branches)

      eff = nevents_cut / nevents
      effs[j][i] = eff

  plt.pcolormesh(x_wps, y_wps, effs, shading='auto')
  plt.colorbar()
  plt.xlabel(x_branch)
  plt.ylabel(y_branch)
  plt.show()

  return selection

def resolved_electron_selection():
  selection = Selection()

  pt_cut = BranchCut('EleLoosePt', 7, 'min')

  max_eta_cut = BranchCut('EleLooseEta', 2.47, 'max')
  uppercrack_eta_cut = BranchCut('EleLooseEta', 1.37, 'max')
  lowercrack_eta_cut = BranchCut('EleLooseEta', 1.52, 'min')
  crack_eta_cut = CompositeCut([uppercrack_eta_cut, lowercrack_eta_cut], 'OR')
  eta_cut = CompositeCut([crack_eta_cut, max_eta_cut], 'AND')

  ele_cut = CompositeCut([eta_cut, pt_cut], 'AND')

  selection.add_cut('Electron', ele_cut)

  return selection

def resolved_muon_selection():
  selection = Selection()

  pt_cut = BranchCut('MuLoosePt', 7, 'min')

  eta_cut = BranchCut('MuLooseEta', 2.7, 'max')

  mu_cut = CompositeCut([pt_cut, eta_cut], 'AND')

  selection.add_cut('Muon', mu_cut)

  return selection

def resolved_tau_selection():
  selection = Selection()

  pt_cut = BranchCut('TauLoosePt', 20, 'min')

  max_eta_cut = BranchCut('TauLooseEta', 2.5, 'max')
  uppercrack_eta_cut = BranchCut('TauLooseEta', 1.37, 'max')
  lowercrack_eta_cut = BranchCut('TauLooseEta', 1.52, 'min')
  crack_eta_cut = CompositeCut([uppercrack_eta_cut, lowercrack_eta_cut], 'OR')
  eta_cut = CompositeCut([crack_eta_cut, max_eta_cut], 'AND')

  one_track_cut = BranchCut('TauLooseNTracks', 1, 'eq')
  three_track_cut = BranchCut('TauLooseNTracks', 3, 'eq')
  track_cut = CompositeCut([one_track_cut, three_track_cut], 'OR')

  pos_charge_cut = BranchCut('TauLooseCharge', 1, 'eq')
  neg_charge_cut = BranchCut('TauLooseCharge', -1, 'eq')
  charge_cut = CompositeCut([pos_charge_cut, neg_charge_cut], 'OR')

  tau_cut = CompositeCut([pt_cut, charge_cut, eta_cut, track_cut], 'AND')

  selection.add_cut('Tau', tau_cut)

  return selection

def resolved_selection():
  selection = Selection()
  
  # Electron cuts for hadel
  nele_resolved_cut = BranchCut('NResolvedElectrons', 1, 'eq')
  nmu_none_resolved_cut = BranchCut('NResolvedMuons', 0, 'eq')
  nele_tight_cut = BranchCut('NEleTight', 1, 'eq')
  leading_ele_tight_pt_cut = BranchCut('LeadingEleTightPt', 25, 'min')

  leading_ele_tight_eta_cut = BranchCut('LeadingEleTightEta', 2.47, 'max')
  leading_ele_low_cracketa_cut = BranchCut('LeadingEleTightEta', 1.37, 'max')
  leading_ele_high_cracketa_cut = BranchCut('LeadingEleTightEta', 1.52, 'min')
  ele_cracketa_cut = CompositeCut([leading_ele_low_cracketa_cut, leading_ele_high_cracketa_cut], 'OR')

  ele_eta_cut = CompositeCut([ele_cracketa_cut, leading_ele_tight_eta_cut], 'AND')
  hadel_cut_list = [nele_tight_cut, nele_resolved_cut, nmu_none_resolved_cut, leading_ele_tight_pt_cut, ele_eta_cut]
  hadel_cut = CompositeCut(hadel_cut_list, 'AND')

  # Muon cuts for hadmu
  nmu_resolved_cut = BranchCut('NResolvedMuons', 1, 'eq')
  nele_none_resolved_cut = BranchCut('NResolvedElectrons', 0, 'eq')
  nmu_medium_cut = BranchCut('NMuMedium', 1, 'eq')
  leading_mu_medium_pt_cut = BranchCut('LeadingMuMediumPt', 21, 'min')
  leading_mu_medium_eta_cut = BranchCut('LeadingMuMediumEta', 2.7, 'max')

  hadmu_cut_list = [nmu_medium_cut, nmu_resolved_cut, nele_none_resolved_cut, leading_mu_medium_pt_cut, leading_mu_medium_eta_cut]
  hadmu_cut = CompositeCut(hadmu_cut_list, 'AND')

  leptons_cut = CompositeCut([hadel_cut, hadmu_cut], 'OR')
  selection.add_cut('Leptons', leptons_cut)

  # Lepton opposite charge
  ele_pos_charge = BranchCut('LeadingEleTightCharge', 1, 'eq')
  tau_neg_charge = BranchCut('LeadingTauLooseCharge', -1, 'eq')
  etau_plus_minus_charge = CompositeCut([ele_pos_charge, tau_neg_charge], 'AND')
  ele_neg_charge = BranchCut('LeadingEleTightCharge', -1, 'eq')
  tau_pos_charge = BranchCut('LeadingTauLooseCharge', 1, 'eq')
  etau_minus_plus_charge = CompositeCut([ele_neg_charge, tau_pos_charge], 'AND')
  eletau_charge_cut = CompositeCut([etau_plus_minus_charge, etau_minus_plus_charge], 'OR')
  
  mu_pos_charge = BranchCut('LeadingMuMediumCharge', 1, 'eq')
  tau_neg_charge = BranchCut('LeadingTauLooseCharge', -1, 'eq')
  mutau_plus_minus_charge = CompositeCut([mu_pos_charge, tau_neg_charge], 'AND')
  mu_neg_charge = BranchCut('LeadingMuMediumCharge', -1, 'eq')
  tau_pos_charge = BranchCut('LeadingTauLooseCharge', 1, 'eq')
  mutau_minus_plus_charge = CompositeCut([mu_neg_charge, tau_pos_charge], 'AND')
  mutau_charge_cut = CompositeCut([mutau_plus_minus_charge, mutau_minus_plus_charge], 'OR')
  
  charge_cut = CompositeCut([eletau_charge_cut, mutau_charge_cut], 'OR')
  selection.add_cut('LeptonCharge', charge_cut)
  
  # Common tau cuts
  ntau_resolved_cut = BranchCut('NResolvedTaus', 1, 'eq')
  ntau_loose_cut = BranchCut('NTauLoose', 1, 'eq')

  leading_tau_loose_pt_cut = BranchCut('LeadingTauLoosePt', 20, 'min')

  leading_tau_ntracks_one_cut = BranchCut('LeadingTauLooseNTracks', 1, 'eq')
  leading_tau_ntracks_three_cut = BranchCut('LeadingTauLooseNTracks', 3, 'eq')
  leading_tau_ntracks_cut = CompositeCut([leading_tau_ntracks_one_cut, leading_tau_ntracks_three_cut], 'OR')

  leading_tau_loose_eta_cut = BranchCut('LeadingTauLooseEta', 2.47, 'max')
  leading_tau_low_cracketa_cut = BranchCut('LeadingTauLooseEta', 1.37, 'max')
  leading_tau_high_cracketa_cut = BranchCut('LeadingTauLooseEta', 1.52, 'min')
  tau_cracketa_cut = CompositeCut([leading_tau_low_cracketa_cut, leading_tau_high_cracketa_cut], 'OR')
  tau_eta_cut = CompositeCut([tau_cracketa_cut, leading_tau_loose_eta_cut], 'AND')

  tau_cut_list = [ntau_resolved_cut, ntau_loose_cut, leading_tau_loose_pt_cut, leading_tau_ntracks_cut, tau_eta_cut]
  tau_cut = CompositeCut(tau_cut_list, 'AND')
  selection.add_cut('Taus', tau_cut)


  # Common jet cuts
  selection.set_cut('NBTagJet', 2, 'min')
  selection.set_cut('LeadingBTagJetPt', 45, 'min')
  selection.set_cut('SubleadingBTagJetPt', 20, 'min')

  # Common dib-jet mass cut
  selection.set_cut('ResolvedDiBJetMass', 150, 'max')

  # Common ditau mass cut
  selection.set_cut('ResolvedMissingMass', 60, 'min')

  return selection

def higg4d2_selection():
  selection = Selection()

  e15_cut = BranchCut('LeadingEleMediumPt', 15, 'min')
  e22_cut = BranchCut('LeadingEleMediumPt', 22, 'min')

  m12_pt_cut = BranchCut('LeadingMuPt', 12, 'min')
  m18_pt_cut = BranchCut('LeadingMuPt', 18, 'min')
  mu_eta_cut = BranchCut('LeadingMuEta', 2.5, 'max')
  mu12_cut = CompositeCut([m12_pt_cut, mu_eta_cut], 'AND') 
  mu18_cut = CompositeCut([m18_pt_cut, mu_eta_cut], 'AND') 

  tau18_cut = BranchCut('LeadingTauPt', 18, 'min')
  tau23_cut = BranchCut('LeadingTauPt', 23, 'min')

  mu18tau18_cut = CompositeCut([mu18_cut, tau18_cut], 'AND') 
  mu12tau23_cut = CompositeCut([mu12_cut, tau23_cut], 'AND') 
  mutau_cut = CompositeCut([mu18tau18_cut, mu12tau23_cut], 'OR')

  e22tau18_cut = CompositeCut([e22_cut, tau18_cut], 'AND')
  e15tau23_cut = CompositeCut([e15_cut, tau23_cut], 'AND')
  etau_cut = CompositeCut([e22tau18_cut, e15tau23_cut], 'OR')
  
  emutau_cut = CompositeCut([mutau_cut, etau_cut], 'OR')

  selection.add_cut('All', emutau_cut)
  return selection
  

class DiTauEfficiencyTracker:
  '''
  A tracker for efficiency of finding correct tau and lepton of ditau system at various points in an algorithm for choosing the object. \
  Chosen tau and lepton are logged at various stages of the flow and a summary is printed when called
  '''
  def __init__(self, branches, channel):
    self.branches = branches
    self.channel = channel
    self.chosen_leptons = {}
    self.chosen_taus = {}
    self.max_dr = 0.1
    self.checkpoint_i = {}
  
  def checkpoint(self, message, lepton, tau):
    if message not in self.chosen_leptons.keys():
      self.chosen_leptons[message] = [0] * len(self.branches)
      self.chosen_taus[message] = [0] * len(self.branches)
      self.checkpoint_i[message] = 0
    self.chosen_leptons[message][self.checkpoint_i[message]] = lepton
    self.chosen_taus[message][self.checkpoint_i[message]] = tau
    self.checkpoint_i[message] += 1

  def summarize(self):
    truth_leptons = create_p4s_branch(self.branches, 'TruthFinalLepton')
    truth_taus = create_p4s_branch(self.branches, 'TruthHadTauVis')
    for message in self.chosen_leptons.keys():
      leptons = tlorentzvectorarray_from_vectors(self.chosen_leptons[message])
      lepton_drs = truth_leptons.delta_r(leptons)
      taus = tlorentzvectorarray_from_vectors(self.chosen_taus[message])
      tau_drs = truth_taus.delta_r(taus)

      lepton_eff = calculate_chosen_truth_efficiencies(truth_leptons, leptons, self.max_dr)
      tau_eff = calculate_chosen_truth_efficiencies(truth_taus, taus, self.max_dr)
      
      print(message)
      print('Lepton efficiency: {:.1%}'.format(lepton_eff))
      print('Tau efficiency: {:.1%}'.format(tau_eff))
      print('')

class BranchCut:
  '''
  A single cut on a single branch

  param branch_name: The name of the branch to be cut on
  param cut_value: Numerical value of the cut
  param cut_type: String denoting whether we are specifying that the cut_value is to be a minimum, maximum, range, or equal of all remaining events after the cut is applied
  '''
  def __init__(self, branch_name, cut_value, cut_type):
    self.branch_name = branch_name
    self.cut_value = cut_value
    self.cut_type = cut_type

  def cut_mask(self, branches):
    # Convert to absolute value if working with eta cut
    if 'Eta' in self.branch_name:
      branch = abs(branches[self.branch_name])
    else:
      branch = branches[self.branch_name]

    if self.cut_type == 'min':
      mask = branch >= self.cut_value
    elif self.cut_type == 'max':
      mask = branch <= self.cut_value
    elif self.cut_type == 'eq':
      mask = branch == self.cut_value
    elif self.cut_type == 'range':
      lower_mask = branch > self.cut_value[0]
      upper_mask = branch < self.cut_value[1]
      mask = ak.all([lower_mask, upper_mask], axis=0)
    if branch.ndim > 1:
      mask = ak.Array([ak.Array([False]) if len(value) == 0 else value for value in mask])
    return mask 

  def event_mask(self, branches):
    cut_mask = self.cut_mask(branches)
    ndim = cut_mask.ndim
    if ndim == 2:
      event_mask = ak.any(cut_mask, axis=1)
    elif ndim == 1:
      event_mask = cut_mask
    else:
      print('No idea what cut_mask is')
    return event_mask

  def cut_nevents(self, branches):
    mask = self.cut_mask(branches)
    nevents = sum(mask)
    return nevents

  def cut_eff(self, branches):
    nevents = self.cut_nevents(branches)
    eff = nevents / len(branches)
    return eff

  def apply_event_cut(self, branches):
    mask = self.event_mask(branches)
    cut_branches = branches[mask]
    return cut_branches

class CompositeCut:
  '''
  A collection of cuts with and/or logic between them

  param cuts: A list of cuts to be combined
  param logic: The logic combining the two cuts
  '''
  def __init__(self, cuts, logic):
    self.cuts = cuts
    self.logic = logic
    self.logic_dict = {'AND' : ak.all,
            'OR' : ak.any
            }
    self.logic_func = self.logic_dict[self.logic]

  def cut_mask(self, branches):
    cut_masks = [cut.cut_mask(branches) for cut in self.cuts]
    cut_mask = self.logic_func(cut_masks, axis=0)
    return cut_mask

  def event_mask(self, branches):
    cut_mask = self.cut_mask(branches)
    ndim = cut_mask.ndim
    if ndim == 2:
      event_mask = ak.any(cut_mask, axis=1)
    elif ndim == 1:
      event_mask = cut_mask
    else:
      print('No idea what cut_mask is')
    return event_mask

  def cut_nevents(self, branches):
    mask = self.event_mask(branches)
    nevents = sum(mask)
    return nevents

  def cut_eff(self, branches):
    nevents = self.cut_nevents(branches)
    eff = nevents / len(branches)
    return eff

  def apply_cut(self, branches):
    mask = self.event_mask(branches)
    cut_branches = branches[mask]
    return cut_branches

class Selection:
  '''
  A collection of cuts that can be applied to branches. Cuts are combined with AND logic but OR can be implemented using CompositeCuts
  '''
  def __init__(self):
    self.cuts = {}
    # This is a default cut so that if no others are defined then all events pass
    self.cuts['BranchCut'] = BranchCut('RunNumber', 0, 'min')

  def make_copy(self):
    copy = deepcopy(self)
    return copy

  def set_cut(self, branch_name, cut_value, min_max_eq, cut_name=None):
    if not cut_name:
      cut_name = branch_name
    self.cuts[cut_name] = BranchCut(branch_name, cut_value, min_max_eq)

  def set_composite_cut(self, cut_name, composite_cut):
    if cut_name in self.cuts.keys():
      print(f'Warning: Cut with name {cut_name} already defined')
    self.cuts[cut_name] = composite_cut

  def add_cut(self, cut_name, cut):
    self.cuts[cut_name] = cut

  def clear_cuts(self):
    self.cuts = {}

  def remove_cut(self, cut_name):
    self.cuts.pop(cut_name)

  def combined_cut_mask(self, branches):
    cut_masks = [cut.event_mask(branches) for cut in self.cuts.values()]
    combined_mask = np.all(cut_masks, axis=0)
    return combined_mask

  def nevents_passing_cuts(self, branches):
    combined_mask = self.combined_cut_mask(branches)
    return sum(combined_mask)

  def cuts_eff(self, branches):
    nevents = self.nevents_passing_cuts(branches)
    eff = nevents / len(branches)
    return eff
  
  def apply_cuts_to_branches(self, branches):
    combined_mask = self.combined_cut_mask(branches)
    return branches[combined_mask]

  def eff_rundown(self, branches):
    for cut_name, cut in self.cuts.items():
      if cut_name == 'BranchCut':
        continue
      cut_eff = cut.cut_eff(branches)
      print(f'{cut_name+":":<25} {cut_eff*100:.2f}%') 

class Sample():
  '''
  A sample corresponding to a single or multiple AODs containing event information, MC weights, and other metadata about the sample. Atomic samples load event info from an NTuple whereas composite samples combine the event info from existing atomic samples.
  '''

  @classmethod
  def composite_sample(cls, is_signal, short_name, sample_list):
    sample = Sample('', is_signal)
    sample.short_name = short_name
    sample.is_composite = 1
    sample.sample_list = sample_list
    return sample

  def __init__(self, ntuple_path, is_signal):
    self.ntuple_path = ntuple_path
    self.is_signal = is_signal

    self.short_name = ''
    self.xsection = 0
    self.luminosity = 0
    self.genfiltereff = 0
    self.source_events = 0
    self.is_skimmed = 0
    self.mc_weight = 0
    self.event_weights = 0
    self.use_mcevent_weights = 0
    self.is_composite = 0

  def __str__(self):
    out = 'Sample: {}\n'.format(self.short_name)
    out += 'Sourced from: {}\n'.format(self.ntuple_path)
    out += 'Events: {}\n'.format(len(self.branches))
    out += 'Source events: {}\n'.format(int(self.source_events))
    out += 'MC weight: {}\n'.format(self.mc_weight)
    return out

  def create_branches(self):
    if self.is_composite:
      branches_list = [sample.branches for sample in self.sample_list]
      self.branches = ak.concatenate(branches_list)
    else:
      self.branches = ntuple_branches_from_path(self.ntuple_path)
    self.create_eff_branches()

  def create_eff_branches(self):
    if self.is_signal:
      self.fid_branches = fiducial_region_cut(self.branches)
      self.hadel_branches = self.branches[self.branches['PDGTruthMatchType'] == 2]
      self.hadmu_branches = self.branches[self.branches['PDGTruthMatchType'] == 3]
      self.fid_hadel_branches = self.fid_branches[self.fid_branches['PDGTruthMatchType'] == 2]
      self.fid_hadmu_branches = self.fid_branches[self.fid_branches['PDGTruthMatchType'] == 3]
      self.eff_branches = self.fid_branches
    else:
      self.eff_branches = deepcopy(self.branches)

  def calculate_mc_weight(self):
    if self.is_skimmed == 0:
      self.source_events = len(self.branches)
    if self.xsection == 0 or self.luminosity == 0 or self.genfiltereff == 0 or self.source_events == 0:
      print('Information not supplied to calculate MC weight')
    else:
      self.mc_weight = (self.xsection * self.luminosity * self.genfiltereff) / self.source_events

  def calculate_event_weights(self):
    # This should do nothing for composite samples
    if self.is_composite:
      return None
    if self.mc_weight == 0:
      self.calculate_mc_weight()
    if self.use_mcevent_weights:
      self.branches['OverallWeight'] = deepcopy(self.branches['EventWeight'])
      self.eff_branches['OverallWeight'] = deepcopy(self.eff_branches['EventWeight'])
    else:
      self.branches['OverallWeight'] = ak.ones_like(np.ones(len(self.branches)))
      self.eff_branches['OverallWeight'] = ak.ones_like(np.ones(len(self.eff_branches)))
    self.branches['OverallWeight'] = self.branches['OverallWeight'] * self.mc_weight
    self.eff_branches['OverallWeight'] = self.eff_branches['OverallWeight'] * self.mc_weight
