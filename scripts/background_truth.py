import ROOT
from glob import glob
ROOT.xAOD.Init()

#f_list = glob('/eos/user/n/nicholas/SWAN_projects/DiTauReco/samples/mc16_13TeV.450524.MadGraphPythia8EvtGen_A14NNPDF23LO_X2000tohh_bbtautau_lephad.recon.AOD.e7244_s3126_r10201/*')
#f_list = glob('/eos/user/n/nicholas/SWAN_projects/DiTauReco/samples/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.recon.AOD.e6337_e5984_s3126_r10201/*')
f_list = glob('/eos/user/n/nicholas/SWAN_projects/DiTauReco/samples/mc16_13TeV.364139.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV280_500_BFilter.recon.AOD.e5313_s3126_r10201/*')
#f_list = glob('/eos/user/n/nicholas/SWAN_projects/DiTauReco/samples/user.nicholas.mc16_13TeV.450166.MadGraphHerwig7EvtGen_PDF23LO_X1600tohh_bbtautau_lephad.recon.AOD.e8317_s3126_r10724_der1632783813/*')

t_chain = ROOT.TChain('CollectionTree')
for f in f_list:
    t_chain.AddFile(f)

t = ROOT.xAOD.MakeTransientTree(t_chain)
print(t.GetListOfBranches().ls())
print(t.GetEntries())

for i, event in enumerate(t):
    print('New event')
    print(len(event.TruthParticles))
    for t_particle in event.TruthParticles:
        t_pdg = t_particle.pdgId()
        if abs(t_pdg) == 6:
            if t_particle.nChildren() > 0:
                first_t_child = t_particle.child(0)
                first_t_child_pdg = first_t_child.pdgId()
                if abs(first_t_child_pdg) != 6:
                    print('Good t')
                    for t_i in range(t_particle.nChildren()):
                        t_child = t_particle.child(t_i)
                        t_child_pdg = t_child.pdgId()
                        print(t_child_pdg)
    for b_particle in event.TruthParticles:
        b_pdg = b_particle.pdgId()
        if abs(b_pdg) == 5:
            if b_particle.nChildren() > 0:
                first_b_child = b_particle.child(0)
                first_b_child_pdg = first_b_child.pdgId()
                if abs(first_b_child_pdg) != 5:
                    print('Good b')
                    for b_i in range(b_particle.nChildren()):
                        b_child = b_particle.child(b_i)
                        b_child_pdg = b_child.pdgId()
    for bm_particle in event.TruthParticles:
        bm_pdg = bm_particle.pdgId()
        if abs(bm_pdg) in [511, 521, 513, 523]:
            if bm_particle.nChildren() > 0:
                first_bm_child = bm_particle.child(0)
                first_bm_child_pdg = first_bm_child.pdgId()
                if abs(first_bm_child_pdg) not in  [511, 521, 513, 523]:
                    print('Good b meson?')
                    for bm_i in range(bm_particle.nChildren()):
                        bm_child = bm_particle.child(bm_i)
                        bm_child_pdg = bm_child.pdgId()
                        print(bm_child_pdg)
    if i == 5:
        break