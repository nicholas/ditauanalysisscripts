import ROOT
import sys
import copy
from helpers import fill_eff_hist, fill_eff_2d_hist, ntuple_particle_list_e, ntuple_particle_list_m
from rootEvent.helpers import nearest_dr_candidate_tlv, build_tlorentz_vector

gen_path = '../samples/ditau_450524_X2000tohh_bbtautau_lephad.recon.AOD.{}.root'
#gen_path = '../samples/ditau_425105_G_hh_4tau_M3000.recon.AOD.{}.root'

f_path = gen_path.format('ntuple')
f = ROOT.TFile(f_path)
t = f.Get('mytree')

print(t.GetListOfBranches().ls())
print(t.GetEntries())

out_path = gen_path.format('plots')
root_out = ROOT.TFile(out_path, 'recreate')

# Plots of counts of various objects
num_eles_hist = ROOT.TH1F('num_eles', 'Number of Electrons', 50, 0, 50)
num_loose_eles_hist = ROOT.TH1F('num_loose_eles', 'Number of Loose Electrons', 50, 0, 50)
num_medium_eles_hist = ROOT.TH1F('num_medium_eles', 'Number of Medium Electrons', 50, 0, 50)
num_tight_eles_hist = ROOT.TH1F('num_tight_eles', 'Number of Tight Electrons', 50, 0, 50)
num_mus_hist = ROOT.TH1F('num_mus', 'Number of Muons', 50, 0, 50)
num_loose_mus_hist = ROOT.TH1F('num_loose_mus', 'Number of Loose Muons', 50, 0, 50)
num_medium_mus_hist = ROOT.TH1F('num_medium_mus', 'Number of Medium Muons', 50, 0, 50)
num_tight_mus_hist = ROOT.TH1F('num_tight_mus', 'Number of Tight Muons', 50, 0, 50)
num_taus_hist = ROOT.TH1F('num_taus', 'Number of Taus', 50, 0, 50)
num_loose_taus_hist = ROOT.TH1F('num_loose_taus', 'Number of Loose Taus', 50, 0, 50)
num_medium_taus_hist = ROOT.TH1F('num_medium_taus', 'Number of Medium Taus', 50, 0, 50)
num_tight_taus_hist = ROOT.TH1F('num_tight_taus', 'Number of Tight Taus', 50, 0, 50)
ele_in_ditau_hist = ROOT.TH1F('ele_in_ditau', 'Electrons With dR < 1.0 of a DiTau', 50, 0, 50)
ele_per_ditau_hist = ROOT.TH1F('ele_per_ditau', 'Electrons With dR < 1.0 per DiTau', 50, 0, 50)
num_ditau_subjets_hist = ROOT.TH1F('num_ditau_subjets', 'Number of HadHad DiTau Subjets', 10, 0, 10)
ditau_ele_subjet_2d_hist = ROOT.TH2F('ditau_ele_subjet_2d', 'Electrons vs. Subjets per DiTau', 50, 0, 50, 10, 0, 10)
num_ditaus_hadhad_hist = ROOT.TH1F('num_ditaus_hadhad', 'Number of HadHad DiTaus', 5, 0, 5)
num_ditaus_hadel_hist = ROOT.TH1F('num_ditaus_hadel', 'Number of HadEl DiTaus', 200, 0, 50)
num_ditaus_hadel_quality_hist = ROOT.TH1F('num_ditaus_hadel_quality', 'Number of HadEl DiTaus (BDT > 0)', 200, 0, 50)
ditau_hadel_bdt_hist = ROOT.TH1F('ditau_hadel_bdt', 'HadEl BDT Score', 100, -5, 5)
ditau_hadel_flat_bdt_hist = ROOT.TH1F('ditau_hadel_flat_bdt', 'HadEl Flat BDT Score', 100, 0, 1)
ed_num_ditaus_hadel_hist = ROOT.TH1F('ed_num_ditaus_hadel', 'Number of HadEl DiTaus (Electron Mode)', 200, 0, 50)
num_ditaus_hadmu_hist = ROOT.TH1F('num_ditaus_hadmu', 'Number of HadMu DiTaus', 200, 0, 50)
md_num_ditaus_hadmu_hist = ROOT.TH1F('md_num_ditaus_hadmu', 'Number of HadMu DiTaus (Muon Mode)', 200, 0, 50)
ele_near_tau_hist = ROOT.TH1F('ele_near_tau', 'HadEl DiTaus (HadMu Style)', 200, 0, 50)

# Variable plots of truth objects
thiggs_pt_hist = ROOT.TH1F('thiggs_pt', 'THiggs True Pt', 100, 0, 2000)
tau_pt_hist = ROOT.TH1F('tau_pt', 'Tau True Pt', 100, 0, 1000)
had_tau_vis_pt_hist = ROOT.TH1F('had_tau_vis_pt', 'Had Tau True Visible Pt', 100, 0, 1000)
had_tau_vis_eta_hist = ROOT.TH1F('had_tau_vis_eta', 'Had Tau True Visible Eta', 50, -5, 5)
had_tau_vis_phi_hist = ROOT.TH1F('had_tau_vis_phi', 'Had Tau True Visible Phi', 40, -4, 4)
had_tau_vis_m_hist = ROOT.TH1F('had_tau_vis_m', 'Had Tau True Visible Mass', 100, 0, 1000)
ditau_hadhad_pt_hist = ROOT.TH1F('ditau_hadhad_pt', 'HadHad DiTau Pt', 100, 0, 2000)
ditau_hadel_pt_hist = ROOT.TH1F('ditau_hadel_pt', 'HadEl DiTau Pt', 100, 0, 1000)
ditau_hadmu_pt_hist = ROOT.TH1F('ditau_hadmu_pt', 'HadMu DiTau Pt', 100, 0, 1000)
final_lepton_pt_hist = ROOT.TH1F('final_lepton_pt', 'Final Lepton True Pt', 100, 0, 500)

# Truth matching counts
???MANY LINES MISSING
???MANY LINES MISSING
???MANY LINES MISSING
???MANY LINES MISSING
???MANY LINES MISSING
???MANY LINES MISSING
    fill_eff_hist(tmtau_vis_eta_hist, tvhadel_ttau_vis_eta_hist, tmtau_vis_eta_eff_hist)
    fill_eff_hist(tvtmhadel_thiggs_pt_hist, tvhadel_thiggs_pt_hist, tvtmhadel_thiggs_pt_eff_hist)
    fill_eff_hist(tvtmhadel_thiggs_eta_hist, tvhadel_thiggs_eta_hist, tvtmhadel_thiggs_eta_eff_hist)
    fill_eff_hist(tvtmhadel_ttau_vis_pt_hist, tvhadel_ttau_vis_pt_hist, tvtmhadel_ttau_vis_pt_eff_hist)
    fill_eff_hist(tvtmhadel_ttau_vis_eta_hist, tvhadel_ttau_vis_eta_hist, tvtmhadel_ttau_vis_eta_eff_hist)
    fill_eff_hist(tvtmhadel_final_lepton_pt_hist, tvhadel_final_lepton_pt_hist, tvtmhadel_final_lepton_pt_eff_hist)
    fill_eff_hist(tvtmhadel_final_lepton_eta_hist, tvhadel_final_lepton_eta_hist, tvtmhadel_final_lepton_eta_eff_hist)
    fill_eff_hist(tvtmhadel_visible_dr_hist, tvhadel_visible_dr_hist, tvtmhadel_visible_dr_eff_hist)
    fill_eff_2d_hist(tvtmhadel_higgs_pt_vs_vis_dr_hist, tvhadel_higgs_pt_vs_vis_dr_hist, tvtmhadel_higgs_pt_vs_vis_dr_eff_hist)
    fill_eff_hist(mantm_ele_tau_visible_dr_hist, tvhadel_visible_dr_hist, mantm_ele_tau_visible_dr_eff_hist)
    fill_eff_hist(mantm_loose_ele_tau_visible_dr_hist, tvhadel_visible_dr_hist, mantm_loose_ele_tau_visible_dr_eff_hist)
    fill_eff_hist(mantm_ele_loose_tau_visible_dr_hist, tvhadel_visible_dr_hist, mantm_ele_loose_tau_visible_dr_eff_hist)
    fill_eff_hist(mantm_loose_ele_loose_tau_visible_dr_hist, tvhadel_visible_dr_hist, mantm_loose_ele_loose_tau_visible_dr_eff_hist)

root_out.Write()
root_out.Close()

