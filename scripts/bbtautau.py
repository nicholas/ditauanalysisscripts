import sys
import ROOT
from glob import glob
from bbtautauClasses.bbtautauClasses import hhbbtautauEvent
from rootEvent.helpers import get_xaod_tree
# Import to import from the below instead of naked rootEvent to get enum class checks to work
from bbtautauClasses.rootEvent.eventClass import Event, Tau, DiTau

ROOT.xAOD.Init()
f_list = glob('/eos/user/n/nicholas/SWAN_projects/bbtautau/truthStudies/dataFiles/mc16_13TeV.450522.MadGraphPythia8EvtGen_A14NNPDF23LO_X1000tohh_bbtautau_lephad.recon.AOD.e7244_s3126_r10201/AOD.16986861._*.pool.root.1')
f_list = f_list[0:1]
t = get_xaod_tree(f_list)
print(f_list)
'''
f_path = '../DiTauLepHadExample/output.root'
f = ROOT.TFile(f_path)
t = f.Get('CollectionTree')
'''
out_path = '../samples/bbtautau.root'
root_out = ROOT.TFile(out_path, 'recreate')
t_out = ROOT.TTree('ntuple', 'mytree')

# Hists holding the number of taus and ditaus of various quality
num_taus_hist = ROOT.TH1F('num_taus', 'Number of Taus', 20, 0, 20)
num_ditaus_hist = ROOT.TH1F('num_ditaus', 'Number of DiTaus', 5, 0, 5)
num_veryloosernn_taus_hist = ROOT.TH1F('num_veryloosernn_taus', 'Number of VeryLooseRNN Taus', 5, 0, 5)
num_veryloosebdt_taus_hist = ROOT.TH1F('num_veryloosebdt_taus', 'Number of VeryLooseBDT Taus', 5, 0, 5)
num_loosebdt_taus_hist = ROOT.TH1F('num_loosebdt_taus', 'Number of LooseBDT Taus', 5, 0, 5)
num_mediumbdt_taus_hist = ROOT.TH1F('num_mediumbdt_taus', 'Number of MediumBDT Taus', 5, 0, 5)
num_tightbdt_taus_hist = ROOT.TH1F('num_tightbdt_taus', 'Number of TightBDT Taus', 5, 0, 5)

# Tau Higgs Pt hists
# Electron decay (ed) mode
ed_thiggs_pt_hist = ROOT.TH1F('ed_thiggs_pt', 'THiggs Pt (Electron Mode)', 100, 0, 1000)
ed_tau_pt_hist = ROOT.TH1F('ed_tau_pt_hist', 'Tau Pt (Electron Mode)', 100, 0, 500)
ed_thiggs_pt_onetau_hist = ROOT.TH1F('ed_thiggs_pt_onetau', 'THiggs Pt (One Tau, Electron Mode)', 100, 0, 1000)
ed_thiggs_pt_twotau_hist = ROOT.TH1F('ed_thiggs_pt_twotau', 'THiggs Pt (Two Taus, Electron Mode)', 100, 0, 1000)
ed_thiggs_pt_onebdtltau_hist = ROOT.TH1F('ed_thiggs_pt_onebdtltau', 'THiggs Pt (One BDT Loose Tau, Electron Mode)', 100, 0, 1000)
ed_thiggs_pt_twobdtltau_hist = ROOT.TH1F('ed_thiggs_pt_twobdtltau', 'THiggs Pt (Two BDT Loose Taus, Electron Mode)', 100, 0, 1000)
ed_thiggs_pt_oneditau_hist = ROOT.TH1F('ed_thiggs_pt_oneditau', 'THiggs Pt (One DiTau, Electron Mode)', 100, 0, 1000)
ed_tau_dr_hist = ROOT.TH1F('ed_tau_dr', 'Tau dR (Electron Mode)', 100, 0, 4)
ed_tau_dr_onetau_hist = ROOT.TH1F('ed_tau_dr_onetau', 'Tau dR (One Tau, Electron Mode)', 100, 0, 4)
ed_tau_dr_twotau_hist = ROOT.TH1F('ed_tau_dr_twotau', 'Tau dR (Two Taus, Electron Mode)', 100, 0, 4)
ed_tau_dr_onebdtltau_hist = ROOT.TH1F('ed_tau_dr_onebdtltau', 'Tau dR (One BDT Loose Tau, Electron Mode)', 100, 0, 4)
ed_tau_dr_twobdtltau_hist = ROOT.TH1F('ed_tau_dr_twobdtltau', 'Tau dR (Two BDT Loose Taus, Electron Mode)', 100, 0, 4)
ed_tau_dr_oneditau_hist = ROOT.TH1F('ed_tau_dr_oneditau', 'Tau dR (One DiTau, Electron Mode)', 100, 0, 4)
# Muon decay (md) mode
md_thiggs_pt_hist = ROOT.TH1F('md_thiggs_pt', 'THiggs Pt (Muon Mode)', 100, 0, 1000)
md_tau_pt_hist = ROOT.TH1F('md_tau_pt_hist', 'Tau Pt (Muon Mode)', 100, 0, 500)
md_thiggs_pt_onetau_hist = ROOT.TH1F('md_thiggs_pt_onetau', 'THiggs Pt (One Tau, Muon Mode)', 100, 0, 1000)
md_thiggs_pt_twotau_hist = ROOT.TH1F('md_thiggs_pt_twotau', 'THiggs Pt (Two Taus, Muon Mode)', 100, 0, 1000)
md_thiggs_pt_onebdtltau_hist = ROOT.TH1F('md_thiggs_pt_onebdtltau', 'THiggs Pt (One BDT Loose Tau, Muon Mode)', 100, 0, 1000)
md_thiggs_pt_twobdtltau_hist = ROOT.TH1F('md_thiggs_pt_twobdtltau', 'THiggs Pt (Two BDT Loose Taus, Muon Mode)', 100, 0, 1000)
md_thiggs_pt_oneditau_hist = ROOT.TH1F('md_thiggs_pt_oneditau', 'THiggs Pt (One DiTau, Muon Mode)', 100, 0, 1000)
md_tau_dr_hist = ROOT.TH1F('md_tau_dr', 'Tau dR (Muon Mode)', 100, 0, 4)
md_tau_dr_onetau_hist = ROOT.TH1F('md_tau_dr_onetau', 'Tau dR (One Tau, Muon Mode)', 100, 0, 4)
md_tau_dr_twotau_hist = ROOT.TH1F('md_tau_dr_twotau', 'Tau dR (Two Taus, Muon Mode)', 100, 0, 4)
md_tau_dr_onebdtltau_hist = ROOT.TH1F('md_tau_dr_onebdtltau', 'Tau dR (One BDT Loose Tau, Muon Mode)', 100, 0, 4)
md_tau_dr_twobdtltau_hist = ROOT.TH1F('md_tau_dr_twobdtltau', 'Tau dR (Two BDT Loose Taus, Muon Mode)', 100, 0, 4)
md_tau_dr_oneditau_hist = ROOT.TH1F('md_tau_dr_oneditau', 'Tau dR (One DiTau, Muon Mode)', 100, 0, 4)

num_events = t.GetEntries()
counter = -1
max_counter = num_events - 1
for event in t:
    counter += 1
    if counter == max_counter:
        break

    my_event = hhbbtautauEvent(event)
    higgs_tautau = my_event.higgs_tautau    
    # Scrap the event if we didn't classify as lephad
    if not higgs_tautau.semihadronic_decay:
        print('Not lephad')
        continue

    # Load reconstructed objects from events
    electrons = my_event.reco_electrons
    muons = my_event.reco_muons
    taus = my_event.tau_jets
    ditaus = my_event.ditau_jets

    looselh_electrons = []
    for electron in electrons:
        el_bool = False
        el_bool = electron.passSelection('LHLoose')
        if bool:
            looselh_electrons.append(electron)
    print('Electrons: ',len(electrons))
    print('LHLoose Electrons: ',len(looselh_electrons))

    veryloosernn_taus = [tau for tau in taus if tau.isTau(28)]
    veryloosebdt_taus = [tau for tau in taus if tau.isTau(18)]
    loosebdt_taus = [tau for tau in taus if tau.isTau(19)]
    mediumbdt_taus = [tau for tau in taus if tau.isTau(20)]
    tightbdt_taus = [tau for tau in taus if tau.isTau(21)]

    num_taus_hist.Fill(len(taus))
    num_veryloosernn_taus_hist.Fill(len(veryloosernn_taus))
    num_veryloosebdt_taus_hist.Fill(len(veryloosebdt_taus))
    num_loosebdt_taus_hist.Fill(len(loosebdt_taus))
    num_mediumbdt_taus_hist.Fill(len(mediumbdt_taus))
    num_tightbdt_taus_hist.Fill(len(tightbdt_taus))
    num_ditaus_hist.Fill(len(ditaus))

    thiggs = higgs_tautau.higgs_truth
    tau = higgs_tautau.tau
    anti_tau = higgs_tautau.anti_tau
    tau_dr = higgs_tautau.children_dr()

    # Tau reconstruction can only happen in eta < 2.5, require truth taus to be in that region   
    if abs(tau.truth.eta()) > 2.5 or abs(anti_tau.truth.eta() > 2.5):
        continue

    # Fill hists of Higgs Pt for variious tau/ditau requirements for efficiencies
    thiggs_pt = thiggs.pt() / 1000.
    tau_pt = tau.truth.pt() / 1000.
    anti_tau_pt = anti_tau.truth.pt() / 1000.
    if my_event.higgs_tautau.leptonic_decay_mode == Tau.LeptonicDecayMode.electron:
        ed_thiggs_pt_hist.Fill(thiggs_pt)
        ed_tau_pt_hist.Fill(tau_pt)
        ed_tau_pt_hist.Fill(anti_tau_pt)
        ed_tau_dr_hist.Fill(tau_dr)
        if len(taus) == 1:
            ed_thiggs_pt_onetau_hist.Fill(thiggs_pt)
            ed_tau_dr_onetau_hist.Fill(tau_dr)
        if len(taus) == 2:
            ed_thiggs_pt_twotau_hist.Fill(thiggs_pt)
            ed_tau_dr_twotau_hist.Fill(tau_dr)
        if len(loosebdt_taus) == 1:
            ed_thiggs_pt_onebdtltau_hist.Fill(thiggs_pt)
            ed_tau_dr_onebdtltau_hist.Fill(tau_dr)
        if len(loosebdt_taus) == 2:
            ed_thiggs_pt_twobdtltau_hist.Fill(thiggs_pt)
            ed_tau_dr_twobdtltau_hist.Fill(tau_dr)
        if len(ditaus) == 1:
            ed_thiggs_pt_oneditau_hist.Fill(thiggs_pt)
            ed_tau_dr_oneditau_hist.Fill(tau_dr)
    elif my_event.higgs_tautau.leptonic_decay_mode == Tau.LeptonicDecayMode.muon:
        md_thiggs_pt_hist.Fill(thiggs_pt)
        md_tau_pt_hist.Fill(tau_pt)
        md_tau_pt_hist.Fill(anti_tau_pt)
        md_tau_dr_hist.Fill(tau_dr)
        if len(taus) == 1:
            md_thiggs_pt_onetau_hist.Fill(thiggs_pt)
            md_tau_dr_onetau_hist.Fill(tau_dr)
        if len(taus) == 2:
            md_thiggs_pt_twotau_hist.Fill(thiggs_pt)
            md_tau_dr_twotau_hist.Fill(tau_dr)
        if len(loosebdt_taus) == 1:
            md_thiggs_pt_onebdtltau_hist.Fill(thiggs_pt)
            md_tau_dr_onebdtltau_hist.Fill(tau_dr)
        if len(loosebdt_taus) == 2:
            md_thiggs_pt_twobdtltau_hist.Fill(thiggs_pt)
            md_tau_dr_twobdtltau_hist.Fill(tau_dr)
        if len(ditaus) == 1:
            md_thiggs_pt_oneditau_hist.Fill(thiggs_pt)
            md_tau_dr_oneditau_hist.Fill(tau_dr)
    else:
        print('Leptonic decay mode not electron or muon')

root_out.Write()
root_out.Close()

