import ROOT

def ntuple_particle_list_e(pt_list, eta_list, phi_list, e_list):
    out_list = []
    for i in range(len(pt_list)):
        part = ROOT.TLorentzVector()
        part.SetPtEtaPhiE(pt_list[i], eta_list[i], phi_list[i], e_list[i])
        out_list.append(part)
    return out_list

def ntuple_particle_list_m(pt_list, eta_list, phi_list, m_list):
    out_list = []
    for i in range(len(pt_list)):
        part = ROOT.TLorentzVector()
        part.SetPtEtaPhiM(pt_list[i], eta_list[i], phi_list[i], e_list[i])
        out_list.append(part)
    return out_list

def fill_eff_hist(num_hist, denom_hist, eff_hist):
    for i in range(eff_hist.GetNbinsX()):
        num_bin = num_hist.GetBinContent(i)
        denom_bin = denom_hist.GetBinContent(i)
        if denom_bin == 0:
            eff_bin = 0
        else:
            eff_bin = num_bin / denom_bin
        eff_hist.SetBinContent(i, eff_bin)

def fill_eff_2d_hist(num_hist, denom_hist, eff_hist):
    for i in range(eff_hist.GetNbinsX()):
        for j in range(eff_hist.GetNbinsY()):
            bin_id = num_hist.GetBin(i, j)
            num_bin = num_hist.GetBinContent(bin_id)
            denom_bin = denom_hist.GetBinContent(bin_id)
            if denom_bin == 0:
                eff_bin = 0
            else:
                eff_bin = num_bin / denom_bin
            eff_hist.SetBinContent(bin_id, eff_bin)

def is_z_to_tautau(particle):
    '''
    Determine whether the given particle is a Z decaying to two taus

    :param particle: TruthParticle to test
    '''
    if not particle.pdgId() == 23 or particle.nChildren() != 2:
        return 0
    tau_ids = [15, -15]
    if particle.child(0).pdgId() not in tau_ids or particle.child(1).pdgId() not in tau_ids:
        return 0
    return 1

def find_z_to_tautau(truth_particles):
    '''
    Return TruthParticle triplet of Z decaying to two taus

    :param truth_particles: List of TruthParticles
    '''
    out = [0, 0, 0]
    for particle in truth_particles:
        if not is_z_to_tautau(particle):
            continue
        out = [particle, particle.child(0), particle.child(1)]
        break
    return out
