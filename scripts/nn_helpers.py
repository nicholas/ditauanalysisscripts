import awkward as ak
import matplotlib.pyplot as plt
import torch
from torch import nn
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from torch.utils.data import DataLoader, Dataset
from sklearn.preprocessing import StandardScaler
from branch_names import branch_names
from flat_samples import samples, even_samples, odd_samples
from tqdm import tqdm

class EventDataset(Dataset):
    def __init__(self, features, labels, extras, transform=None, target_transform=None):
        self.features = features
        self.labels = labels
        self.extras = extras
        self.transform = transform
        self.target_transform = target_transform
        
    def __len__(self):
        return len(self.features)
    
    def __getitem__(self, idx):
        event_features = self.features[idx]
        event_label = self.labels[idx]
        event_extras = self.extras[idx]
        if self.transform:
            event_features = self.transform(event_features)
        if self.target_transform:
            event_label = self.target_transform(event_label)
        return event_features, event_label, event_extras

class DFValues:
    def __init__(self, dataloader, model, device, branch_names):
        self.dataloader = dataloader
        self.model = model
        self.branch_names = branch_names
        self.all_vals = all_vals_df(dataloader, model, device, self.branch_names)
        self.signal_vals, self.background_vals = split_by_signal(self.all_vals, self.branch_names)
        self.max_vals, self.nonmax_vals = split_by_max(self.all_vals, self.branch_names)
        self.max_signal_vals, self.max_background_vals = split_by_signal(self.max_vals, self.branch_names)
        self.nonmax_signal_vals, self.nonmax_background_vals = split_by_signal(self.nonmax_vals, self.branch_names)
        self.duplicate_id_vals = self.all_vals[self.all_vals.duplicated(branch_names.id_branch_name, keep=False)]

class NNEventData:
    random_seed = 6
    batch_size = 64
    test_size=0.2

    def __init__(self, branch_names):
        self.branch_names = branch_names

    def train_test_split(self, test_size, random_state):
        self.X_train, self.X_test, self.y_train, self.y_test, self.extra_train, self.extra_test = train_test_split(self.inputs, self.labels, self.extras, test_size=test_size, random_state=random_state)
        
    def prepare_training_data(self):
        self.train_test_split(self.test_size, self.random_seed)
        self.train_dataset = EventDataset(self.X_train, self.y_train, self.extra_train)
        self.test_dataset = EventDataset(self.X_test, self.y_test, self.extra_test)
        self.train_dataloader = DataLoader(self.train_dataset, batch_size=self.batch_size, shuffle=True)
        self.test_dataloader = DataLoader(self.test_dataset, batch_size=self.batch_size, shuffle=True)
        
    def prepare_full_data(self):
        self.dataset = EventDataset(self.inputs, self.labels, self.extras)
        self.dataloader = DataLoader(self.dataset, batch_size=self.batch_size, shuffle=False)
        
    def test_df(self, model, device):
        return DFValues(self.test_dataloader, model, device, self.branch_names)
    
    def train_df(self, model, device):
        return DFValues(self.train_dataloader, model, device, self.branch_names)
    
    def df(self, model, device):
        return DFValues(self.dataloader, model, device, self.branch_names)

class AdHocData(NNEventData):
    def __init__(self, sample, branch_names, scaler=None):
        super().__init__(branch_names)
        self.sample = sample
        if not scaler:
            scaler = StandardScaler()
        self.scaler = scaler

    def split_data(self, training):
        inputs = np.array(self.sample[self.branch_names.input_branch_names])
        inputs = np.array([np.array(list(row)) for row in inputs])
        if training:
            self.scaler.fit(inputs)
            self.inputs = self.scaler.transform(inputs)
        else:
            self.inputs = self.scaler.transform(inputs)

        self.labels = np.array(self.sample[self.branch_names.label_branch_name])
        
        extras = np.array(self.sample[self.branch_names.extra_branch_names])
        self.extras = np.array([np.array(list(row)) for row in extras])

class SampleData(NNEventData):
    def __init__(self, samples, sample_names, branch_names):
        super().__init__(branch_names)
        self.samples = samples
        self.sample_names = sample_names
        self.scaler = StandardScaler()
        self.inputs, self.labels, self.extras = split_samples(self.samples, self.sample_names, branch_names, self.scaler, 1)

    def split_samples(self, branch_names):
        self.branch_names = branch_names
        self.inputs, self.labels, self.extras, self.scaler = split_samples(self.samples, self.sample_names, branch_names, self.scaler, 1)
            

class NeuralNetwork(nn.Module):
    def __init__(self, branch_names, classifier_yn=True, activation=nn.ReLU()):
        super(NeuralNetwork, self).__init__()
        self.input_size = len(branch_names.input_branch_names)
        self.classifier_yn = classifier_yn
        self.branch_names = branch_names
        self.activation = activation
        
    def forward(self, x):
        logit = self.linear_stack(x)
        return logit    

class NeuralNetworkV0(NeuralNetwork):
    def __init__(self, branch_names):
        super().__init__(branch_names)
        self.linear_stack = nn.Sequential(
            nn.Linear(self.input_size, 2 * self.input_size),
            nn.ReLU(),
            nn.Linear(2 * self.input_size, 3 * self.input_size),
            nn.ReLU(),
            nn.Linear(3 * self.input_size, 3 * self.input_size),
            nn.ReLU(),
            nn.Linear(3 * self.input_size, 3 * self.input_size),
            nn.ReLU(),
            nn.Linear(3 * self.input_size, 2 * self.input_size),
            nn.ReLU(),
            nn.Linear(2 * self.input_size, 1),
        )

class NeuralNetworkV1(NeuralNetwork):
    def __init__(self, branch_names, dropout=0.2):
        super().__init__(branch_names)
        self.dropout = dropout
        self.linear_stack = nn.Sequential(
            nn.Linear(self.input_size, 2 * self.input_size),
            nn.Dropout(self.dropout),
            nn.ReLU(),
            nn.Linear(2 * self.input_size, 3 * self.input_size),
            nn.Dropout(self.dropout),
            nn.ReLU(),
            nn.Linear(3 * self.input_size, 3 * self.input_size),
            nn.Dropout(self.dropout),
            nn.ReLU(),
            nn.Linear(3 * self.input_size, 3 * self.input_size),
            nn.Dropout(self.dropout),
            nn.ReLU(),
            nn.Linear(3 * self.input_size, 2 * self.input_size),
            nn.Dropout(self.dropout),
            nn.ReLU(),
            nn.Linear(2 * self.input_size, 1),
        )

class NeuralNetworkV2(NeuralNetwork):
    def __init__(self, branch_names, activation):
        super().__init__(branch_names, activation=activation)
        self.dropout = 0.2
        self.linear_stack = nn.Sequential(
            nn.Linear(self.input_size, 2 * self.input_size),
            self.activation,
            nn.Linear(2 * self.input_size, 3 * self.input_size),
            self.activation,
            nn.Linear(3 * self.input_size, 3 * self.input_size),
            self.activation,
            nn.Linear(3 * self.input_size, 3 * self.input_size),
            self.activation,
            nn.Linear(3 * self.input_size, 3 * self.input_size),
            nn.Dropout(self.dropout),
            self.activation,
            nn.Linear(3 * self.input_size, 2 * self.input_size),
            self.activation,
            nn.Linear(2 * self.input_size, 1),
        )

def train_loop(dataloader, model, loss_fn, optimizer, device):
    size = len(dataloader.dataset)
    num_batches = len(dataloader)
    printer = 1
    total_loss, correct = 0, 0

    model.train()
    model.to(device)
    for batch, (X, y, _) in enumerate(dataloader):
        #y = y[:,0]
        X, y = X.to(device), y.to(device)
        y = torch.unsqueeze(y, 1)
        # Compute prediction and loss 
        model = model.float()
        pred = model(X.float())

        loss = loss_fn(pred, y.float())
        
        # Backpropagation
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        
        #if batch % 10000 == 0:
        #    loss, current = loss.item(), batch * len(X)
        #    print(f'loss: {loss:>7f} [{current:>5d}/{size:>5d}]')

        if model.classifier_yn:
          pred_bin = pred.sigmoid().round()
        else:
          pred_bin = pred.round()

        correct += (pred_bin == y).type(torch.float).sum().item()
        if not isinstance(loss, float):
            batch_loss = loss.item()
        else:
            batch_loss = loss
        total_loss += batch_loss
        
    total_loss /= num_batches
    correct /= size
    #print(f'Train Error: \n Accuracy: {100*correct:>8f}%, Avg loss: {total_loss:>8f} \n')
    return total_loss, correct

def test_loop(dataloader, model, loss_fn, device, train_mode=0):
    size = len(dataloader.dataset)
    num_batches = len(dataloader)
    test_loss, correct = 0, 0
    
    if train_mode:
      model.train()
    else:
      model.eval()
    with torch.no_grad():
        for X, y, _ in dataloader:
            #y = y[:,0]
            X, y = X.to(device), y.to(device)
            y = torch.unsqueeze(y, 1)
            pred = model(X.float())

            if model.classifier_yn:
              pred_bin = pred.sigmoid().round()
            else:
              pred_bin = pred.round()

            test_loss += loss_fn(pred, y.float()).item()
            correct += (pred_bin == y).type(torch.float).sum().item()
            
    test_loss /= num_batches
    correct /= size
    #print(f'Test Error: \n Accuracy: {100*correct:>8f}%, Avg loss: {test_loss:>8f} \n')
    return test_loss, correct

def train_nn(data, model, loss, optimizer, device, epochs):
    train_losses, test_losses = [], []
    train_accs, test_accs = [], []
    # First get the performance on datasets without any training
    # Test dataset
    test_loss, test_acc = test_loop(data.test_dataloader, model, loss, device)
    test_losses.append(test_loss)
    test_accs.append(test_acc)
    # Train dataset, test_loop here is intentional
    train_loss, train_acc = test_loop(data.train_dataloader, model, loss, device)
    train_losses.append(train_loss)
    train_accs.append(train_acc)
    #print('Starting loop')
    for t in tqdm(range(epochs)):
        #print(f'Epoch {t+1}\n--------------------------')
        train_loss, train_acc = train_loop(data.train_dataloader, model, loss, optimizer, device)
        test_loss, test_acc = test_loop(data.test_dataloader, model, loss, device)
        train_losses.append(train_loss)
        train_accs.append(train_acc)
        test_losses.append(test_loss)
        test_accs.append(test_acc)
    print('Done!')
    
    plt.rcParams['figure.figsize'] = [12,6] 
    plt.rcParams['figure.dpi'] = 150
    
    x = np.arange(epochs+1)
    plt.plot(x, train_accs, label='Train')
    plt.plot(x, test_accs, label='Test')
    plt.legend()
    plt.title('Accuracy vs Epoch')
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy')
    plt.show()
    
    x = np.arange(epochs+1)
    plt.plot(x, train_losses, label='Train')
    plt.plot(x, test_losses, label='Test')
    plt.legend()
    plt.title('Loss vs Epoch')
    plt.xlabel('Epoch')
    plt.ylabel('Loss')
    plt.show()

def pred_tensor(dataloader, model, device):
    preds = [0] * len(dataloader)
    inputs = [0] * len(dataloader)
    truths = [0] * len(dataloader)
    extras = [0] * len(dataloader)
    model.to(device)
    with torch.no_grad():
        for i, (X, y, z) in enumerate(dataloader):
            X, y = X.to(device), y.to(device)
            pred = model(X.float())
            preds[i] = torch.squeeze(pred, 1)
            inputs[i] = X
            truths[i] = y
            extras[i] = z
    extras = tuple(extras)
    extras = torch.cat(extras, dim=0)
    truths = tuple(truths)
    truths = torch.cat(truths, dim=0)   
    inputs = tuple(inputs)
    inputs = torch.cat(inputs, dim=0)
    preds = tuple(preds)
    preds = torch.cat(preds, dim=0)
    if model.classifier_yn:
        preds = preds.sigmoid()
    return preds, inputs, truths, extras

def pred_vals(dataloader, model, device, binary=0):
    preds, inputs, truths, extras = pred_tensor(dataloader, model, device)
    if binary:
        preds = preds.round()
    out_preds, out_inputs, out_truths, out_extras = preds.detach().cpu(), inputs.detach().cpu(), truths.detach().cpu(), extras.detach().cpu()
    return np.array(out_preds), np.array(out_inputs), np.array(out_truths), np.array(out_extras)

def all_vals_df(dataloader, model, device, branch_names):
    preds, inputs, truth, extras = pred_vals(dataloader, model, device)
    
    pred_pd = pd.DataFrame(preds, columns=[branch_names.output_branch_name])

    inputs_pd = pd.DataFrame(inputs, columns=branch_names.input_branch_names).astype('float')
    input_column_renaming = {}
    for name in branch_names.input_branch_names:
      input_column_renaming[name] = 'Norm'+name
    inputs_pd = inputs_pd.rename(columns=input_column_renaming)

    truth_pd = pd.DataFrame(truth, columns=[branch_names.label_branch_name])

    extra_column_formats = {}
    for name in branch_names.extra_branch_names:
      if name in ['UniqueEventID', 'SampleID']:
        column_format = 'int'
      else:
        column_format = 'float'
      extra_column_formats[name] = column_format
    extra_pd = pd.DataFrame(extras, columns=branch_names.extra_branch_names).astype(extra_column_formats)
    
    all_pd = pd.concat([pred_pd, inputs_pd, truth_pd, extra_pd], axis=1)
    
    return all_pd

def plot_predictions(sample_id_dict, dataloader, model, device):
    preds_prob, inputs_prob, truth_prob, extras_prob = pred_vals(dataloader, model, device)
    
    plt.rcParams['figure.figsize'] = [12,6] 
    plt.rcParams['figure.dpi'] = 150
    figs, axs = plt.subplots(1, 2)

    sig_prob = preds_prob[truth_prob == 1]
    back_prob = preds_prob[truth_prob == 0]
    axs[0].hist(sig_prob, alpha=0.5, label='TM')
    axs[0].hist(back_prob, alpha=0.5, label='Non-TM')
    axs[0].set_xlabel('Prediction Value')
    axs[0].set_xlim([0, 1])
    axs[0].legend()

    sample_id_prob = extras_prob[:,1]
    for name, id_val in sample_id_dict.items():
        sample_prob = preds_prob[sample_id_prob == id_val]
        
        sample_events = len(sample_prob)
        if sample_events == 0:
            continue
        
        axs[1].hist(sample_prob, alpha=0.3, label=name)
        
    axs[1].set_xlabel('Prediction Value')
    axs[1].set_xlim([0, 1])
    axs[1].legend()
    plt.show()

def plot_predictions_by_truth(dataloader, model):
    preds_prob, _, truth_prob, _ = pred_vals(dataloader, model)
    preds_bin, _, truth_bin, _ = pred_vals(dataloader, model, binary=1)
    sig_prob = preds_prob[truth_prob == 1]
    back_prob = preds_prob[truth_prob == 0]
    sig_bin = preds_bin[truth_bin == 1]
    back_bin = preds_bin[truth_bin == 0]
    
    plt.rcParams['figure.figsize'] = [12,6] 
    plt.rcParams['figure.dpi'] = 150
    figs, axs = plt.subplots(1, 2)
    axs[0].hist(sig_prob, alpha=0.5, label='Signal')
    axs[0].hist(back_prob, alpha=0.5, label='Background')
    axs[0].set_title('Prediction')
    axs[0].set_xlim([0, 1.2])
    axs[0].legend()
    axs[1].hist(sig_bin, alpha=0.5, label='Signal')
    axs[1].hist(back_bin, alpha=0.5, label='Background')
    axs[1].set_title('Binary')
    axs[1].set_xlim([0, 1.2])
    axs[1].legend()
    plt.show()
    
def plot_predictions_by_sample(sample_id_dict, dataloader, model):
    preds_prob, inputs_prob, truth_prob, extras_prob = pred_vals(dataloader, model)
    preds_bin, inputs_bin, truth_bin, extras_bin = pred_vals(dataloader, model, binary=1)
    plt.rcParams['figure.figsize'] = [12,6] 
    plt.rcParams['figure.dpi'] = 150
    figs, axs = plt.subplots(1, 2)
    sample_id_prob = extras_prob[:,1]
    sample_id_bin = extras_bin[:,1]
    
    for name, id_val in sample_id_dict.items():
        sample_prob = preds_prob[sample_id_prob == id_val]
        sample_bin = preds_bin[sample_id_bin == id_val]
        preds_sample_bin = preds_bin[sample_id_bin == id_val]
        signal_bin = sample_bin[preds_sample_bin == 1]
        back_bin = sample_bin[preds_sample_bin == 0]
        
        sample_events = len(sample_bin)
        if sample_events == 0:
            continue
        
        axs[0].hist(sample_prob, alpha=0.3, label=name)
        axs[1].hist(sample_bin, alpha=0.3, label=name)
        
        print(f'{name} events: {sample_events}')
        print(f'Signal: {len(signal_bin) / sample_events:.1%}')
        print(f'Background: {len(back_bin) / sample_events:.1%}')

    axs[0].set_title('Predictions')
    axs[0].set_xlim([0, 1])
    axs[0].legend()
    
    axs[1].set_title('Binary')
    axs[1].set_xlim([0, 1])
    axs[1].legend()
    
    plt.show()    

def plot_best_system_pred(df, train_string='', evaluate_string=''):
    plt.rcParams['figure.figsize'] = [12,6] 
    plt.rcParams['figure.dpi'] = 150
    plt.hist(df.max_signal_vals['Pred'], bins=50, alpha=0.5, label='TM', density=True)
    plt.hist(df.max_background_vals['Pred'], bins=50, alpha=0.5, label='Non-TM', density=True)
    plt.title('NN Output per Event')
    if train_string or evaluate_string:
        plt.text(0.6, 18, f'{train_string}\n{evaluate_string}')
    plt.legend()
    plt.xlabel('NN Output')
    plt.ylabel('AU')
    plt.show()

def split_samples(samples, sample_names, branch_names, scaler, training=1):
    input_branches_list = [sample.branches[branch_names.input_branch_names] for name, sample in samples.items() if name in sample_names]
    labels_list = [sample.branches[branch_names.label_branch_name] for name, sample in samples.items() if name in sample_names]
    extras_list = [sample.branches[branch_names.extra_branch_names] for name, sample in samples.items() if name in sample_names]
    
    input_branches = ak.concatenate(input_branches_list)
    labels = ak.concatenate(labels_list)
    extra_branches = ak.concatenate(extras_list)
    
    input_np = np.array(input_branches)
    input_np = np.array([np.array(list(row)) for row in input_np])
    if training:
        scaler.fit(input_np)
        input_np = scaler.transform(input_np)

    label_np = np.array(labels)

    extra_np = np.array(extra_branches)
    extra_np = np.array([np.array(list(row)) for row in extra_np])
    
    return input_np, label_np, extra_np

def split_by_max(df, branch_names):
    '''
    Split up a DataFrame object by the rows that have the maximum output branch (NN output) value for that event. First groups by ID branch (EventNum) then by IsElectron to break ties in the case that both systems are given the same output value.

    param df: DataFrame with one row per event-system
    param branch_names: BranchNames object
    '''

    # First find the system with the maximum output score per event
    df_grouped = df.groupby(branch_names.id_branch_name)
    df_grouped_pred = df_grouped[branch_names.output_branch_name].transform(max)
    idx = df_grouped_pred == df[branch_names.output_branch_name]
    df_max = df[idx]

    # Next choose the electron system if both systems in an event have the same output score
    df_max_ie_grouped = df_max.groupby(branch_names.id_branch_name)
    df_max_ie = df_max_ie_grouped[branch_names.tiebreaker_branch_name].transform(max)
    ie_idx = df_max_ie == df_max[branch_names.tiebreaker_branch_name]
    df_max = df_max[ie_idx]

    df_nonmax = pd.concat([df, df_max]).drop_duplicates(keep=False)
    return df_max, df_nonmax

def split_by_signal(df, branch_names):
    df_signal = df[df[branch_names.label_branch_name] == 1]
    df_background = df[df[branch_names.label_branch_name] == 0]
    return df_signal, df_background

def populate_nn_results(branches, model, scaler, model_name=None):
    '''
    Add info to branches containing the results of applying a trained NN model to the branches

    :param branches: Branches containing all necessary inputs to model
    :param model: Trained NN model
    :param scaler: Scaler object that was used to train NN model
    :param model_name: Extra string to append to default name for filled branches
    '''
    output_branch_name = 'NNPred' + model_name
    lepton_pts = []
    lepton_etas = []
    lepton_phis = []
    lepton_es = []
    tau_pts = []
    tau_etas = []
    tau_phis = []
    tau_es = []
    leptau_pts = []
    leptau_etas = []
    leptau_phis = []
    leptau_es = []
    leptau_ms = []
    jet_pts = []
    jet_etas = []
    jet_phis = []
    jet_es = []
    jet_ms = []
    dihiggs_ms = []
    is_electron = []
    is_signal = []
    event_num = []
    event_id = []

    for i, event_branches in enumerate(branches):
        if event_branches['NEleLoose'] > 0:
            lepton_pts.append(event_branches['HadElChosenElePt'])
            lepton_etas.append(event_branches['HadElChosenEleEta'])
            lepton_phis.append(event_branches['HadElChosenElePhi'])
            lepton_es.append(event_branches['HadElChosenEleE'])
            tau_pts.append(event_branches['HadElChosenTauPt'])
            tau_etas.append(event_branches['HadElChosenTauEta'])
            tau_phis.append(event_branches['HadElChosenTauPhi'])
            tau_es.append(event_branches['HadElChosenTauE'])
            leptau_pts.append(event_branches['HadElChosenTauElePt'])
            leptau_etas.append(event_branches['HadElChosenTauEleEta'])
            leptau_phis.append(event_branches['HadElChosenTauElePhi'])
            leptau_es.append(event_branches['HadElChosenTauEleE'])
            leptau_ms.append(event_branches['HadElChosenTauEleM'])
            jet_pts.append(event_branches['HadElLargeRHbbJetPt'])
            jet_etas.append(event_branches['HadElLargeRHbbJetEta'])
            jet_phis.append(event_branches['HadElLargeRHbbJetPhi'])
            jet_es.append(event_branches['HadElLargeRHbbJetE'])
            jet_ms.append(event_branches['HadElLargeRHbbJetM'])
            dihiggs_ms.append(event_branches['HadElDiHiggsM'])
            is_electron.append(1)
            is_signal.append(1)
            event_num.append(i)
            event_id.append(event_branches['UniqueEventID'])
        if event_branches['NMuTight'] > 0:
            lepton_pts.append(event_branches['HadMuChosenMuPt'])
            lepton_etas.append(event_branches['HadMuChosenMuEta'])
            lepton_phis.append(event_branches['HadMuChosenMuPhi'])
            lepton_es.append(event_branches['HadMuChosenMuE'])
            tau_pts.append(event_branches['HadMuChosenTauPt'])
            tau_etas.append(event_branches['HadMuChosenTauEta'])
            tau_phis.append(event_branches['HadMuChosenTauPhi'])
            tau_es.append(event_branches['HadMuChosenTauE'])
            leptau_pts.append(event_branches['HadMuChosenTauMuPt'])
            leptau_etas.append(event_branches['HadMuChosenTauMuEta'])
            leptau_phis.append(event_branches['HadMuChosenTauMuPhi'])
            leptau_es.append(event_branches['HadMuChosenTauMuE'])
            leptau_ms.append(event_branches['HadMuChosenTauMuM'])
            jet_pts.append(event_branches['HadMuLargeRHbbJetPt'])
            jet_etas.append(event_branches['HadMuLargeRHbbJetEta'])
            jet_phis.append(event_branches['HadMuLargeRHbbJetPhi'])
            jet_es.append(event_branches['HadMuLargeRHbbJetE'])
            jet_ms.append(event_branches['HadMuLargeRHbbJetM'])
            dihiggs_ms.append(event_branches['HadMuDiHiggsM'])
            is_electron.append(0)
            is_signal.append(1)
            event_num.append(i)
            event_id.append(event_branches['UniqueEventID'])

    framer = pd.DataFrame.from_dict({
                                     'LeptonPt' : lepton_pts,
                                     'LeptonEta' : lepton_etas,
                                     'LeptonPhi' : lepton_phis,
                                     'LeptonE' : lepton_es,
                                     'TauPt' : tau_pts,
                                     'TauEta' : tau_etas,
                                     'TauPhi' : tau_phis,
                                     'TauE' : tau_es,
                                     'LepTauPt' : leptau_pts,
                                     'LepTauEta' : leptau_etas,
                                     'LepTauPhi' : leptau_phis,
                                     'LepTauE' : leptau_es,
                                     'LepTauM' : leptau_ms,
                                     'LargeRJetPt' : jet_pts,
                                     'LargeRJetEta' : jet_etas,
                                     'LargeRJetPhi' : jet_phis,
                                     'LargeRJetE' : jet_es,
                                     'LargeRJetM' : jet_ms,
                                     'DiHiggsM' : dihiggs_ms,
                                     'IsElectron' : is_electron,
                                     'IsSignal' : is_signal,
                                     'EventNum' : event_num,
                                     'UniqueEventID' : event_id
                                    }
                                   )

    adhoc_sample = AdHocData(framer, model.branch_names, scaler)
    adhoc_sample.split_data(0)
    adhoc_sample.prepare_full_data()
    results = adhoc_sample.df(model, 'cpu')
    max_vals = results.max_vals

    branches['IsElectron'] = max_vals['IsElectron']
    # Now that we know whether to choose the electron or muon system fill useful values from that system
    collapse_channel_branch(branches, model_name, 'DiHiggsM', 'HadElDiHiggsM', 'HadMuDiHiggsM')
    collapse_channel_branch(branches, model_name, 'HbbDisc', 'HadElLargeRHbbJetDiscriminant', 'HadMuLargeRHbbJetDiscriminant')
    branches[output_branch_name] = max_vals['Pred']    

def collapse_channel_branch(branches, model_name, branch_name, hadel_name, hadmu_name):
    branch_name = branch_name + model_name
    branches[branch_name] = [event[hadel_name] if event['IsElectron'] else event[hadmu_name] for event in branches]

def collapse_model_branch(branches, branch_name, model_name):
    even_branch_name = branch_name + model_name + 'Even'
    odd_branch_name = branch_name + model_name + 'Odd'
    fold_branch_name = branch_name + model_name + 'Fold'

    branches[fold_branch_name] = [event[odd_branch_name] if event['EventNumber']%2==0 else event[even_branch_name] for event in branches]

def save_trained_branches(sample, branch_names):
    for branch_name in branch_names:
        save_pd = ak.to_pandas(sample.branches[branch_name])
        save_pd.to_pickle(f'../trained_models/{sample.did_name}_{branch_name}.pkl')

def load_trained_branches(sample, branch_names):
    for branch_name in branch_names:
        branch = ak.Array(pd.read_pickle(f'../trained_models/{sample.did_name}_{branch_name}.pkl')['values'])
        sample.branches[branch_name] = branch
    # Propagate new branches to eff_branches
    sample.create_eff_branches()
