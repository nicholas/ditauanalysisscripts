import ROOT
import array
from sample_naming import SampleNaming, sample_naming_dict
import argparse
import sys

parser = argparse.ArgumentParser()
parser.add_argument('sample', type=str)
parser.add_argument('version', type=str)
parser.add_argument('--nevents', type=int, default=-1, required=False)
args = vars(parser.parse_args())
sample_name = args['sample']
version = args['version']
nevents = args['nevents']

if sample_name in ['X1000', 'X1600', 'X2000']:
  is_signal_sample = 1
else:
  is_signal_sample = 0

in_naming_kwargs = SampleNaming.format_kwargs(skim_yn=1, version=version, flat_yn=0)
sample_naming = sample_naming_dict[sample_name]
input_path = sample_naming.file_path(in_naming_kwargs)
print(input_path)
f_in = ROOT.TFile(input_path)
t_in = f_in.Get('mytree')
print(t_in.GetEntries())

out_naming_kwargs = SampleNaming.format_kwargs(skim_yn=1, version=version, flat_yn=1)
out_name = sample_naming.ntuple_name(out_naming_kwargs)
print(out_name)

f_out = ROOT.TFile(out_name, 'RECREATE')
mytree = ROOT.TTree('mytree', 'mytree')

RunNumber = array.array('i', [0])
EventNumber = array.array('i', [0])
UniqueEventID = array.array('L', [0])
LeptonPt = array.array('f', [0])
LeptonEta = array.array('f', [0])
LeptonPhi = array.array('f', [0])
LeptonE = array.array('f', [0])
LeptonTruthdR = array.array('f', [0])
TauPt = array.array('f', [0])
TauEta = array.array('f', [0])
TauPhi = array.array('f', [0])
TauE = array.array('f', [0])
TauTruthdR = array.array('f', [0])
LepTauPt = array.array('f', [0])
LepTauEta = array.array('f', [0])
LepTauPhi = array.array('f', [0])
LepTauE = array.array('f', [0])
LepTauM = array.array('f', [0])
LargeRJetPt = array.array('f', [0])
LargeRJetEta = array.array('f', [0])
LargeRJetPhi = array.array('f', [0])
LargeRJetE = array.array('f', [0])
LargeRJetM = array.array('f', [0])
LargeRJetTruthdR = array.array('f', [0])
DiHiggsM = array.array('f', [0])
isElectron = array.array('i', [0])
isSignal = array.array('i', [0])
isSignalCont = array.array('f', [0])

mytree.Branch('RunNumber', RunNumber, 'RunNumber/I') 
mytree.Branch('EventNumber', EventNumber, 'EventNumber/I') 
mytree.Branch('UniqueEventID', UniqueEventID, 'UniqueEventID/L') 
mytree.Branch('LeptonPt', LeptonPt, 'LeptonPt/F') 
mytree.Branch('LeptonEta', LeptonEta, 'LeptonEta/F') 
mytree.Branch('LeptonPhi', LeptonPhi, 'LeptonPhi/F') 
mytree.Branch('LeptonE', LeptonE, 'LeptonE/F') 
mytree.Branch('LeptonTruthdR', LeptonTruthdR, 'LeptonTruthdR/F') 
mytree.Branch('TauPt', TauPt, 'TauPt/F') 
mytree.Branch('TauEta', TauEta, 'TauEta/F') 
mytree.Branch('TauPhi', TauPhi, 'TauPhi/F') 
mytree.Branch('TauE', TauE, 'TauE/F') 
mytree.Branch('TauTruthdR', TauTruthdR, 'TauTruthdR/F') 
mytree.Branch('LepTauPt', LepTauPt, 'LepTauPt/F') 
mytree.Branch('LepTauEta', LepTauEta, 'LepTauEta/F') 
mytree.Branch('LepTauPhi', LepTauPhi, 'LepTauPhi/F') 
mytree.Branch('LepTauE', LepTauE, 'LepTauE/F') 
mytree.Branch('LepTauM', LepTauM, 'LepTauM/F') 
mytree.Branch('LargeRJetPt', LargeRJetPt, 'LargeRJetPt/F') 
mytree.Branch('LargeRJetEta', LargeRJetEta, 'LargeRJetEta/F') 
mytree.Branch('LargeRJetPhi', LargeRJetPhi, 'LargeRJetPhi/F') 
mytree.Branch('LargeRJetE', LargeRJetE, 'LargeRJetE/F') 
mytree.Branch('LargeRJetM', LargeRJetM, 'LargeRJetM/F') 
mytree.Branch('LargeRJetTruthdR', LargeRJetTruthdR, 'LargeRJetTruthdR/F') 
mytree.Branch('DiHiggsM', DiHiggsM, 'DiHiggsM/F')
mytree.Branch('IsElectron', isElectron, 'IsElectron/I') 
mytree.Branch('IsSignal', isSignal, 'IsSignal/I') 
mytree.Branch('IsSignalCont', isSignalCont, 'IsSignalCont/F') 

def dr_is_signal(reco_dr, min_truth_dr):
  if reco_dr < min_truth_dr:
    is_signal = 1
  elif reco_dr >= min_truth_dr and reco_dr < (2*min_truth_dr):
    m = -1 / (2*min_truth_dr)
    b = 1 - (min_truth_dr * m)
    is_signal = m * reco_dr + b
  else:
    is_signal = 0
  return is_signal

lep_min_dr = 0.1
largerjet_min_dr = 0.2
event_counter = 0
entry_counter = 0
events_with_entries_counter = 0
truth_event_counter = 0
for i, event in enumerate(t_in):
  event_counter += 1
  found_truth = 0
  found_entry = 0
  #unique_event_id = build_unique_event_id(event.EventNumber, event.LeadingLargeRJetPt, event.LeadingEleLoosePt, event.LeadingMuTightPt)
  if event.HadElChosenElePt > 0 and event.HadElChosenTauPt > 0 and event.HadElLargeRHbbJetPt > 0:
    RunNumber[0] = event.RunNumber
    EventNumber[0] = event.EventNumber
    UniqueEventID[0] = event.UniqueEventID
    LeptonPt[0] = event.HadElChosenElePt
    LeptonEta[0] = event.HadElChosenEleEta
    LeptonPhi[0] = event.HadElChosenElePhi
    LeptonE[0] = event.HadElChosenEleE
    LeptonTruthdR[0] = event.HadElChosenEleTruthdR
    TauPt[0] = event.HadElChosenTauPt
    TauEta[0] = event.HadElChosenTauEta
    TauPhi[0] = event.HadElChosenTauPhi
    TauE[0] = event.HadElChosenTauE
    TauTruthdR[0] = event.HadElChosenTauTruthdR
    LepTauPt[0] = event.HadElChosenTauElePt
    LepTauEta[0] = event.HadElChosenTauEleEta
    LepTauPhi[0] = event.HadElChosenTauElePhi
    LepTauE[0] = event.HadElChosenTauEleE
    LepTauM[0] = event.HadElChosenTauEleM
    LargeRJetPt[0] = event.HadElLargeRHbbJetPt
    LargeRJetEta[0] = event.HadElLargeRHbbJetEta
    LargeRJetPhi[0] = event.HadElLargeRHbbJetPhi
    LargeRJetE[0] = event.HadElLargeRHbbJetE
    LargeRJetM[0] = event.HadElLargeRHbbJetM
    LargeRJetTruthdR[0] = event.HadElLargeRHbbJetTruthdR
    DiHiggsM[0] = event.HadElDiHiggsM
    isElectron[0] = 1
    if is_signal_sample and event.PDGTruthMatchType == 2 and event.HadElChosenEleTruthdR < lep_min_dr and event.HadElChosenTauTruthdR < lep_min_dr and event.HadElLargeRHbbJetTruthdR < largerjet_min_dr:
        isSignal[0] = 1
        found_truth = 1
    else:
      isSignal[0] = 0
    if is_signal_sample and event.PDGTruthMatchType == 2:
        ele_is_signal = dr_is_signal(event.HadElChosenEleTruthdR, lep_min_dr)
        tau_is_signal = dr_is_signal(event.HadElChosenTauTruthdR, lep_min_dr)
        jet_is_signal = dr_is_signal(event.HadElLargeRHbbJetTruthdR, largerjet_min_dr)
        isSignalCont[0] = (ele_is_signal + tau_is_signal + jet_is_signal) / 3
    found_entry = 1
    entry_counter += 1
    mytree.Fill()

  if event.HadMuChosenMuPt > 0 and event.HadMuChosenTauPt > 0 and event.HadMuLargeRHbbJetPt > 0:
    RunNumber[0] = event.RunNumber
    EventNumber[0] = event.EventNumber
    UniqueEventID[0] = event.UniqueEventID
    LeptonPt[0] = event.HadMuChosenMuPt
    LeptonEta[0] = event.HadMuChosenMuEta
    LeptonPhi[0] = event.HadMuChosenMuPhi
    LeptonE[0] = event.HadMuChosenMuE
    LeptonTruthdR[0] = event.HadMuChosenMuTruthdR
    TauPt[0] = event.HadMuChosenTauPt
    TauEta[0] = event.HadMuChosenTauEta
    TauPhi[0] = event.HadMuChosenTauPhi
    TauE[0] = event.HadMuChosenTauE
    TauTruthdR[0] = event.HadMuChosenTauTruthdR
    LepTauPt[0] = event.HadMuChosenTauMuPt
    LepTauEta[0] = event.HadMuChosenTauMuEta
    LepTauPhi[0] = event.HadMuChosenTauMuPhi
    LepTauE[0] = event.HadMuChosenTauMuE
    LepTauM[0] = event.HadMuChosenTauMuM
    LargeRJetPt[0] = event.HadMuLargeRHbbJetPt
    LargeRJetEta[0] = event.HadMuLargeRHbbJetEta
    LargeRJetPhi[0] = event.HadMuLargeRHbbJetPhi
    LargeRJetE[0] = event.HadMuLargeRHbbJetE
    LargeRJetM[0] = event.HadMuLargeRHbbJetM
    LargeRJetTruthdR[0] = event.HadMuLargeRHbbJetTruthdR
    DiHiggsM[0] = event.HadMuDiHiggsM
    isElectron[0] = 0
    if is_signal_sample and event.PDGTruthMatchType == 3 and event.HadMuChosenMuTruthdR < lep_min_dr and event.HadMuChosenTauTruthdR < lep_min_dr and event.HadMuLargeRHbbJetTruthdR < largerjet_min_dr:
      isSignal[0] = 1
      found_truth = 1
    else:
      isSignal[0] = 0
    if is_signal_sample and event.PDGTruthMatchType == 3:
        mu_is_signal = dr_is_signal(event.HadMuChosenMuTruthdR, lep_min_dr)
        tau_is_signal = dr_is_signal(event.HadMuChosenTauTruthdR, lep_min_dr)
        jet_is_signal = dr_is_signal(event.HadMuLargeRHbbJetTruthdR, largerjet_min_dr)
        isSignalCont[0] = (mu_is_signal + tau_is_signal + jet_is_signal) / 3
    found_entry = 1
    entry_counter += 1
    mytree.Fill()
  truth_event_counter += found_truth
  events_with_entries_counter += found_entry
  if i == nevents:
    break
    
print('Total events')
print(event_counter)
print('Truth events found')
print(truth_event_counter)
print('Total entries')
print(entry_counter)
print('Events with entries')
print(events_with_entries_counter)

f_out.Write()
f_out.Close()
