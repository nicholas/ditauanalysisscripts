import ROOT
import argparse

ROOT.gROOT.SetBatch()
ROOT.gROOT.SetStyle('Modern')

parser = argparse.ArgumentParser()
parser.add_argument('sample', type=str)
args = vars(parser.parse_args())
sample = args['sample']

f_path = '../samples/tauEff{}.root'.format(sample)

f = ROOT.TFile(f_path)

# Get our histograms
tau_pt_vs_dr_hist = f.Get('tau_pt_vs_dr')
tmtau_tau_pt_vs_dr_hist = f.Get('tmtau_tau_pt_vs_dr')
tau_pt_vs_dr_eff_hist = f.Get('tau_pt_vs_dr_eff')

tau_pt_vs_dr_ele_hist = f.Get('tau_pt_vs_dr_ele')
tmtau_tau_pt_vs_dr_ele_hist = f.Get('tmtau_tau_pt_vs_dr_ele')
tau_pt_vs_dr_ele_eff_hist = f.Get('tau_pt_vs_dr_ele_eff')

tau_pt_vs_dr_mu_hist = f.Get('tau_pt_vs_dr_mu')
tmtau_tau_pt_vs_dr_mu_hist = f.Get('tmtau_tau_pt_vs_dr_mu')
tau_pt_vs_dr_mu_eff_hist = f.Get('tau_pt_vs_dr_mu_eff')

print_path = '../samples/tauEff{}Plots.pdf'.format(sample)

c1 = ROOT.TCanvas('c1')
c1.Print(print_path + '[')

tau_pt_vs_dr_hist.Draw('colz')
tau_pt_vs_dr_hist.SetStats(0)
c1.Print(print_path)

tmtau_tau_pt_vs_dr_hist.Draw('colz')
tmtau_tau_pt_vs_dr_hist.SetStats(0)
c1.Print(print_path)

c1.SetRightMargin(0.15)
tau_pt_vs_dr_eff_hist.SetTitle('Visible Tau Pt vs Tau-Lep dR Efficiency;Visible Tau p_{T};Visible Tau - Electron dR;Tau Reco Efficiency')
tau_pt_vs_dr_eff_hist.Draw('colz')
tau_pt_vs_dr_eff_hist.SetStats(0)
ROOT.gPad.Modified()
ROOT.gPad.Update()
pt1 = ROOT.TPaveText(0.65, 0.75, 0.8, 0.82, 'NDC NB')
t1 = pt1.AddText("X #rightarrow HH #rightarrow bb#tau_{h}#tau_{l}")
pt1.AddText("X @ 2 TeV")
pt1.AddText("e channel")
pt1.Draw()
pt1.SetFillColor(0)
c1.Print(print_path)

tau_pt_vs_dr_ele_hist.Draw('colz')
tau_pt_vs_dr_ele_hist.SetStats(0)
c1.Print(print_path)

tmtau_tau_pt_vs_dr_ele_hist.Draw('colz')
tmtau_tau_pt_vs_dr_ele_hist.SetStats(0)
c1.Print(print_path)

c1.SetRightMargin(0.15)
tau_pt_vs_dr_ele_eff_hist.SetTitle('Visible Tau Pt vs Tau-Ele dR Efficiency (Electron Mode);Visible Tau p_{T};Visible Tau - Electron dR;Tau Reco Efficiency')
tau_pt_vs_dr_ele_eff_hist.Draw('colz')
tau_pt_vs_dr_ele_eff_hist.SetStats(0)
ROOT.gPad.Modified()
ROOT.gPad.Update()
pt1 = ROOT.TPaveText(0.65, 0.75, 0.8, 0.82, 'NDC NB')
t1 = pt1.AddText("X #rightarrow HH #rightarrow bb#tau_{h}#tau_{l}")
pt1.AddText("X @ 2 TeV")
pt1.AddText("e channel")
pt1.Draw()
pt1.SetFillColor(0)
c1.Print(print_path)

tau_pt_vs_dr_mu_hist.Draw('colz')
tau_pt_vs_dr_mu_hist.SetStats(0)
c1.Print(print_path)

tmtau_tau_pt_vs_dr_mu_hist.Draw('colz')
tmtau_tau_pt_vs_dr_mu_hist.SetStats(0)
c1.Print(print_path)

c1.SetRightMargin(0.15)
tau_pt_vs_dr_mu_eff_hist.SetTitle('Visible Tau Pt vs Tau-Mu dR Efficiency (Muon Mode);Visible Tau p_{T};Visible Tau - Electron dR;Tau Reco Efficiency')
tau_pt_vs_dr_mu_eff_hist.Draw('colz')
tau_pt_vs_dr_mu_eff_hist.SetStats(0)
ROOT.gPad.Modified()
ROOT.gPad.Update()
pt1 = ROOT.TPaveText(0.65, 0.75, 0.8, 0.82, 'NDC NB')
t1 = pt1.AddText("X #rightarrow HH #rightarrow bb#tau_{h}#tau_{l}")
pt1.AddText("X @ 2 TeV")
pt1.AddText("e channel")
pt1.Draw()
pt1.SetFillColor(0)
c1.Print(print_path)

c1.Print(print_path + ']')

