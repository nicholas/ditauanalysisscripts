from ZTauTauClasses import ZTauTauEvent
from bbtautauClasses.bbtautauClasses import hhbbtautauEvent
import rootEvent.eventClass
from rootEvent.helpers import nearest_branch_member, nearest_branch_member_tlv, build_tlorentz_vector
from glob import glob
import ROOT
import sys
import copy
import math
import numpy as np
from array import array
import argparse
import datetime

start_time = datetime.datetime.now()
print('Starting: ',start_time)

parser = argparse.ArgumentParser()
parser.add_argument('sample', type=str)
args = vars(parser.parse_args())
sample = args['sample']

ROOT.xAOD.Init()

if sample == 'Ztautau':
    f_list = glob('/eos/user/n/nicholas/SWAN_projects/DiTauReco/samples/mc16_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.recon.AOD.e3601_s3126_r11634/*')
    higgs_pt_max = 500
    tau_pt_max = 250
    true_tau_dr_max = 5
    vis_tau_final_lep_dr_max = 1
    ftm_tau_pt_low = 0
    ftm_tau_pt_high = 5000
    out_path = '../samples/tauEffZtautau.root'
elif sample == '1000':
    f_list = glob('/eos/user/n/nicholas/SWAN_projects/DiTauReco/samples/mc16_13TeV.450522.MadGraphPythia8EvtGen_A14NNPDF23LO_X1000tohh_bbtautau_lephad.recon.AOD.e7244_s3126_r10201/*')
    higgs_pt_max = 1000
    tau_pt_max = 1000
    true_tau_dr_max = 3
    vis_tau_final_lep_dr_max = 1
    ftm_tau_pt_low = 0
    ftm_tau_pt_high = 5000
    out_path = '../samples/tauEff1000.root'
elif sample == '2000':
    f_list = glob('/eos/user/n/nicholas/SWAN_projects/DiTauReco/samples/mc16_13TeV.450524.MadGraphPythia8EvtGen_A14NNPDF23LO_X2000tohh_bbtautau_lephad.recon.AOD.e7244_s3126_r10201/*')
    higgs_pt_max = 1500
    tau_pt_max = 1500
    true_tau_dr_max = 3
    vis_tau_final_lep_dr_max = 1
    ftm_tau_pt_low = 0
    ftm_tau_pt_high = 5000
    out_path = '../samples/tauEff2000.root'
elif sample == '3000':
    f_list = glob('/eos/user/n/nicholas/SWAN_projects/DiTauReco/samples/mc16_13TeV.425105.MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_4tau_c10_M3000.recon.AOD.e6072_s3126_r10724/*.root.1')
    higgs_pt_max = 2000
    tau_pt_max = 2000
    true_tau_dr_max = 3
    vis_tau_final_lep_dr_max = 1
    ftm_tau_pt_low = 0
    ftm_tau_pt_high = 5000
    out_path = '../samples/tauEff3000.root'
high_dr = 0.4
high_pt = 500

t_chain = ROOT.TChain('CollectionTree')
for f in f_list:
    t_chain.AddFile(f)

t = ROOT.xAOD.MakeTransientTree(t_chain)

root_out = ROOT.TFile(out_path, 'recreate')

num_tau_hist = ROOT.TH1F('num_tau', 'Number of Reco Taus', 50, 0, 50)
had_taus_hist = ROOT.TH1F('had_taus', 'Number of True Hadronic Taus', 5, 0, 5)
reco_tau_pt_hist = ROOT.TH1F('reco_tau_pt', 'Reco Tau Pt', 100, 0, 1000)
higgs_pt_hist = ROOT.TH1F('higgs_pt', 'Higgs Pt', 100, 0, 2000)
tau_vis_theta_hist = ROOT.TH1F('tau_vis_theta', 'Tau Visible Theta', 50, 0, 1)
tau_nu_theta_hist = ROOT.TH1F('tau_nu_theta', 'Tau Neutrino Theta', 50, 0, 1)

higgs_pt_hist = ROOT.TH1F('higgs_pt', 'Higgs Pt', 100, 0, higgs_pt_max)
tmtau_higgs_pt_hist = ROOT.TH1F('tmtau_higgs_pt', 'Higgs Pt TM', 100, 0, higgs_pt_max)
higgs_pt_eff_hist = ROOT.TH1F('higgs_pt_eff', 'Higgs Pt Efficiency', 100, 0, higgs_pt_max)

tau_pt_hist = ROOT.TH1F('tau_pt', 'Tau Pt', 100, 0, tau_pt_max)
tmtau_pt_hist = ROOT.TH1F('tmtau_pt', 'Tau Pt TM', 100, 0, tau_pt_max)
tau_pt_eff_hist = ROOT.TH1F('tau_pt_eff', 'Tau Pt Efficiency', 100, 0, tau_pt_max)

high_dr_tau_pt_hist = ROOT.TH1F('high_dr_tau_pt', 'Tau Pt (dR > {})'.format(high_dr), 100, 0, tau_pt_max)
tmtau_high_dr_tau_pt_hist = ROOT.TH1F('tmtau_high_dr_tau_pt', 'Tau Pt TM (dR > {})'.format(high_dr), 100, 0, tau_pt_max)
high_dr_tau_pt_eff_hist = ROOT.TH1F('high_dr_tau_pt_eff', 'Tau Pt Efficiency (dR > {})'.format(high_dr), 100, 0, tau_pt_max)

high_pt_dr_hist = ROOT.TH1F('high_pt_dr', 'Visible Tau - Final Lepton dR (pT > {})'.format(high_pt), 100, 0, vis_tau_final_lep_dr_max)
tmtau_high_pt_dr_hist = ROOT.TH1F('tmtau_high_pt_dr', 'Visible Tau - Final Lepton dR TM (pT > {})'.format(high_pt), 100, 0, vis_tau_final_lep_dr_max)
high_pt_dr_eff_hist = ROOT.TH1F('high_pt_dr_eff', 'Visible Tau - Final Lepton dR Efficiency (pT > {})'.format(high_pt), 100, 0, vis_tau_final_lep_dr_max)

tau_pt_ele_hist = ROOT.TH1F('tau_pt_ele', 'Tau Pt (Electron Mode)', 100, 0, tau_pt_max)
tmtau_pt_ele_hist = ROOT.TH1F('tmtau_pt_ele', 'Tau Pt TM (Electron Mode)', 100, 0, tau_pt_max)
tau_pt_ele_eff_hist = ROOT.TH1F('tau_pt_ele_eff', 'Tau Pt Efficiency (Electron Mode)', 100, 0, tau_pt_max)

tau_pt_mu_hist = ROOT.TH1F('tau_pt_mu', 'Tau Pt (Muon Mode)', 100, 0, tau_pt_max)
tmtau_pt_mu_hist = ROOT.TH1F('tmtau_pt_mu', 'Tau Pt TM (Muon Mode)', 100, 0, tau_pt_max)
tau_pt_mu_eff_hist = ROOT.TH1F('tau_pt_mu_eff', 'Tau Pt Efficiency (Muon Mode)', 100, 0, tau_pt_max)

tau_eta_hist = ROOT.TH1F('tau_eta', 'Tau Eta', 100, -5, 5)
tmtau_eta_hist = ROOT.TH1F('tmtau_eta', 'Tau Eta TM', 100, -5, 5)
tau_eta_eff_hist = ROOT.TH1F('tau_eta_eff', 'Tau Eta Efficiency', 100, -5, 5)

tau_phi_hist = ROOT.TH1F('tau_phi', 'Tau Phi', 100, -5, 5)
tmtau_phi_hist = ROOT.TH1F('tmtau_phi', 'Tau Phi TM', 100, -5, 5)
tau_phi_eff_hist = ROOT.TH1F('tau_phi_eff', 'Tau Phi Efficiency', 100, -5, 5)

tau_vis_theta_star_hist = ROOT.TH1F('tau_vis_theta_star', 'Tau Visible Theta Star', 50, 0, 5)
tmtau_vis_theta_star_hist = ROOT.TH1F('tmtau_vis_theta_star', 'Tau Visible Theta Star TM', 50, 0, 5)
tau_vis_theta_star_eff_hist = ROOT.TH1F('tau_vis_theta_star_eff', 'Tau Visible Theta Star Efficiency', 50, 0, 5)

tau_nu_theta_star_hist = ROOT.TH1F('tau_nu_theta_star', 'Tau Neutrino Theta Star', 50, 0, 5)
tmtau_nu_theta_star_hist = ROOT.TH1F('tmtau_nu_theta_star', 'Tau Nu Theta Star TM', 50, 0, 5)
tau_nu_theta_star_eff_hist = ROOT.TH1F('tau_nu_theta_star_eff', 'Tau Nu Theta Star Efficiency', 50, 0, 5)

truth_tau_nearest_dr_hist = ROOT.TH1F('truth_tau_nearest_dr', 'Nearest Other Truth Tau dR', 100, 0, true_tau_dr_max)
tmtau_truth_tau_nearest_dr_hist = ROOT.TH1F('tmtau_truth_tau_nearest_dr', 'Nearest Other Truth Tau dR TM', 100, 0, true_tau_dr_max)
truth_tau_nearest_dr_eff_hist = ROOT.TH1F('truth_tau_nearest_dr_eff', 'Nearest Other Truth Tau dR Efficiency', 100, 0, true_tau_dr_max)

vis_tau_final_lep_dr_hist = ROOT.TH1F('vis_tau_final_lep_dr', 'Visible Tau - Final Lepton dR', 100, 0, vis_tau_final_lep_dr_max)
tmtau_vis_tau_final_lep_dr_hist = ROOT.TH1F('tmtau_vis_tau_final_lep_dr', 'Visible Tau - Final Lepton dR TM', 100, 0, vis_tau_final_lep_dr_max)
vis_tau_final_lep_dr_eff_hist = ROOT.TH1F('vis_tau_final_lep_dr_eff', 'Visible Tau - Final Lepton dR Efficiency', 100, 0, vis_tau_final_lep_dr_max)

vis_tau_final_lep_dr_ele_hist = ROOT.TH1F('vis_tau_final_lep_dr_ele', 'Visible Tau - Final Lepton dR (Electron Mode)', 100, 0, vis_tau_final_lep_dr_max)
tmtau_vis_tau_final_lep_dr_ele_hist = ROOT.TH1F('tmtau_vis_tau_final_lep_dr_ele', 'Visible Tau - Final Lepton dR TM (Electron Mode)', 100, 0, vis_tau_final_lep_dr_max)
vis_tau_final_lep_dr_ele_eff_hist = ROOT.TH1F('vis_tau_final_lep_dr_ele_eff', 'Visible Tau - Final Lepton dR Efficiency (Electron Mode)', 100, 0, vis_tau_final_lep_dr_max)

vis_tau_final_lep_dr_mu_hist = ROOT.TH1F('vis_tau_final_lep_dr_mu', 'Visible Tau - Final Lepton dR (Muon Mode)', 100, 0, vis_tau_final_lep_dr_max)
tmtau_vis_tau_final_lep_dr_mu_hist = ROOT.TH1F('tmtau_vis_tau_final_lep_dr_mu', 'Visible Tau - Final Lepton dR TM (Muon Mode)', 100, 0, vis_tau_final_lep_dr_max)
vis_tau_final_lep_dr_mu_eff_hist = ROOT.TH1F('vis_tau_final_lep_dr_mu_eff', 'Visible Tau - Final Lepton dR Efficiency (Muon Mode)', 100, 0, vis_tau_final_lep_dr_max)

vis_tau_final_lep_deta_hist = ROOT.TH1F('vis_tau_final_lep_deta', 'Visible Tau - Final Lepton dEta', 100, 0, vis_tau_final_lep_dr_max)
tmtau_vis_tau_final_lep_deta_hist = ROOT.TH1F('tmtau_vis_tau_final_lep_deta', 'Visible Tau - Final Lepton dEta TM', 100, 0, vis_tau_final_lep_dr_max)
vis_tau_final_lep_deta_eff_hist = ROOT.TH1F('vis_tau_final_lep_deta_eff', 'Visible Tau - Final Lepton dEta Efficiency', 100, 0, vis_tau_final_lep_dr_max)

vis_tau_final_lep_dphi_hist = ROOT.TH1F('vis_tau_final_lep_dphi', 'Visible Tau - Final Lepton dPhi', 100, 0, vis_tau_final_lep_dr_max)
tmtau_vis_tau_final_lep_dphi_hist = ROOT.TH1F('tmtau_vis_tau_final_lep_dphi', 'Visible Tau - Final Lepton dPhi TM', 100, 0, vis_tau_final_lep_dr_max)
vis_tau_final_lep_dphi_eff_hist = ROOT.TH1F('vis_tau_final_lep_dphi_eff', 'Visible Tau - Final Lepton dPhi Efficiency', 100, 0, vis_tau_final_lep_dr_max)

vis_tau_closest_b_dr_hist = ROOT.TH1F('vis_tau_closest_b_dr', 'Visible Tau - Closest b-Quark dR', 100, 0, 5)
tmtau_vis_tau_closest_b_dr_hist = ROOT.TH1F('tmtau_vis_tau_closest_b_dr', 'Visible Tau - Closest b-Quark dR TM', 100, 0, 5)
vis_tau_closest_b_dr_eff_hist = ROOT.TH1F('vis_tau_closest_b_dr_eff', 'Visible Tau - Closest b-Quark dR Efficiency', 100, 0, 5)

tau_pt_vs_dr_hist = ROOT.TH2F('tau_pt_vs_dr', 'Visible Tau Pt vs Tau-Lep dR', 100, 0, tau_pt_max, 100, 0, vis_tau_final_lep_dr_max)
tmtau_tau_pt_vs_dr_hist = ROOT.TH2F('tmtau_tau_pt_vs_dr', 'Visible Tau Pt vs Tau-Lep dR TM', 100, 0, tau_pt_max, 100, 0, vis_tau_final_lep_dr_max)
tau_pt_vs_dr_eff_hist = ROOT.TH2F('tau_pt_vs_dr_eff', 'Visible Tau Pt vs Tau-Lep dR Efficiency', 100, 0, tau_pt_max, 100, 0, vis_tau_final_lep_dr_max)

tau_pt_vs_dr_ele_hist = ROOT.TH2F('tau_pt_vs_dr_ele', 'Visible Tau Pt vs Tau-Ele dR (Electron Mode)', 100, 0, tau_pt_max, 100, 0, vis_tau_final_lep_dr_max)
tmtau_tau_pt_vs_dr_ele_hist = ROOT.TH2F('tmtau_tau_pt_vs_dr_ele', 'Visible Tau Pt vs Tau-Ele dR TM (Electron Mode)', 100, 0, tau_pt_max, 100, 0, vis_tau_final_lep_dr_max)
tau_pt_vs_dr_ele_eff_hist = ROOT.TH2F('tau_pt_vs_dr_ele_eff', 'Visible Tau Pt vs Tau-Ele dR Efficiency (Electron Mode)', 100, 0, tau_pt_max, 100, 0, vis_tau_final_lep_dr_max)

tau_pt_vs_dr_mu_hist = ROOT.TH2F('tau_pt_vs_dr_mu', 'Visible Tau Pt vs Tau-Mu dR (Muon Mode)', 100, 0, tau_pt_max, 100, 0, vis_tau_final_lep_dr_max)
tmtau_tau_pt_vs_dr_mu_hist = ROOT.TH2F('tmtau_tau_pt_vs_dr_mu', 'Visible Tau Pt vs Tau-Mu dR TM (Muon Mode)', 100, 0, tau_pt_max, 100, 0, vis_tau_final_lep_dr_max)
tau_pt_vs_dr_mu_eff_hist = ROOT.TH2F('tau_pt_vs_dr_mu_eff', 'Visible Tau Pt vs Tau-Mu dR Efficiency (Muon Mode)', 100, 0, tau_pt_max, 100, 0, vis_tau_final_lep_dr_max)

reco_tau_nearest_dr_hist = ROOT.TH1F('reco_tau_nearest_dr', 'Tau TM Nearest dR', 100, -1, 1)

ftm_had_taus_hist = ROOT.TH1F('ftm_had_taus', 'Number of True Hadronic Taus (Failed TM)', 5, 0, 5)
ftm_tau_pt_hist = ROOT.TH1F('ftm_tau_pt', 'Tau Pt (Failed TM)', 100, 0, 500)
ftm_tau_eta_hist = ROOT.TH1F('ftm_tau_eta', 'Tau Eta (Failed TM)', 100, -5, 5)
ftm_tau_phi_hist = ROOT.TH1F('ftm_tau_phi', 'Tau Phi (Failed TM)', 100, -5, 5)
ftm_num_tau_hist = ROOT.TH1F('ftm_num_tau', 'Number of Reco Taus (Failed TM)', 50, 0, 50)
ftm_reco_tau_pt_hist = ROOT.TH1F('ftm_reco_tau_pt', 'Reco Tau Pt', 100, 0, 1000)
ftm_tau_vis_theta_star_hist = ROOT.TH1F('ftm_tau_vis_theta_star', 'Tau Visible Theta Star (Failed TM)', 50, 0, 5)
ftm_tau_nu_theta_star_hist = ROOT.TH1F('ftm_tau_nu_theta_star', 'Tau Nu Theta Star (Failed TM)', 50, 0, 5)

tau_nu_theta_star_list = np.array([], dtype=np.float32)
tau_vis_theta_star_list = np.array([], dtype=np.float32)

#print(t.GetListOfBranches().ls())
num_entries = t.GetEntries()
print('Number of entries: ',num_entries)
counter = -1
#max_counter = num_entries - 1
max_counter = 20000

## Loop ##
for event in t:
    #print('New event')
    #print('---------')
    counter += 1
    if counter == max_counter:
        break

    # Loop through truth particles for Higgs
    higgs = 0
    for truth_part in event.TruthParticles:
        if truth_part.pdgId() == 25:
            has_tau_child = 0
            has_antitau_child = 0
            for i in range(truth_part.nChildren()):
                child = truth_part.child(i)
                child_pdg = child.pdgId()
                if child_pdg == 15:
                    has_tau_child = 1
                if child_pdg == -15:
                    has_antitau_child = 1
            if has_tau_child and has_antitau_child:
                higgs = truth_part

    if not higgs:
        continue

    # Loop through truth particles for had tau, lep tau, and final lepton
    num_tau_hist.Fill(len(event.TauJets))
    failed_match = 0
    lep_taus = []
    final_leptons = []
    had_taus = []
    has_electron = 0
    has_muon = 0
    for truth_part in event.TruthParticles:
        if truth_part.pdgId() in [15, -15]:
            #print('New tau')
            lep_tau = 0
            had_tau = 1
            lepton_child = 0
            if truth_part.nChildren() == 0:
                continue
            for i in range(truth_part.nChildren()):
                child = truth_part.child(i)
                child_pdg = child.pdgId()
                if child_pdg in [15, -15]:
                    lep_tau = 0
                    had_tau = 0
                    continue
                if child_pdg in [11, -11, 13, -13]:
                    had_tau = 0
                    lep_tau = 1
                    lepton_child = child
                    if child_pdg in [11, -11]:
                        has_electron = 1
                    else:
                        has_muon = 1
                    continue
            if not had_tau and not lep_tau:
                continue
            if lep_tau:
                lep_taus.append(truth_part)
                final_leptons.append(lepton_child)
            elif had_tau:
                had_taus.append(truth_part)
            else:
                continue

    # Loop over truth particles and find b quarks
    b_quarks = []
    for truth_part in event.TruthParticles:
        if truth_part.pdgId() in [5, -5]:
            real_b = 1
            for child_i in range(truth_part.nChildren()):
                child = truth_part.child(child_i)
                if child.pdgId() in [5, -5]:
                    real_b = 0
            if real_b:
                b_quarks.append(truth_part)

    if len(b_quarks) != 2:
        continue

    if len(had_taus) != 1:
        continue
    had_taus_hist.Fill(len(had_taus))

    for tau in had_taus:
        # Build vectors for had tau and find nearest other truth tau
        closest_truth_tau, closest_truth_tau_dr = nearest_branch_member(tau, lep_taus)
        final_lepton_i = lep_taus.index(closest_truth_tau)
        final_lepton = final_leptons[final_lepton_i]
        final_lepton_tlv = build_tlorentz_vector(final_lepton)
        
        # Build vectors for the tau neutrino and visible decay products
        tau_vis_tlv = ROOT.TLorentzVector()
        tau_vis_tlv.SetPtEtaPhiE(0, 0, 0, 0)
        for i in range(tau.nChildren()):
            child = tau.child(i)
            child_pdg = child.pdgId()
            if child_pdg in [16, -16]:
                tau_nu = child
            else:
                vis_child = build_tlorentz_vector(child)
                tau_vis_tlv += vis_child

        # Calculate dR between visible tau and final lepton
        vis_tau_final_lep_dr = tau_vis_tlv.DeltaR(final_lepton_tlv)
        vis_tau_final_lep_deta = abs(tau_vis_tlv.Eta() - final_lepton_tlv.Eta())
        vis_tau_final_lep_dphi = abs(tau_vis_tlv.Phi() - final_lepton_tlv.Phi())

        # Build vector for nearest b quark
        closest_b_quark_tlv, vis_tau_closest_b_dr = nearest_branch_member_tlv(tau_vis_tlv, b_quarks)

        # Calculate theta and theta star variables
        tau_tlv = build_tlorentz_vector(tau)
        tau_tlv_boost = tau_tlv.BoostVector()
        tau_vis_ref_tlv = copy.deepcopy(tau_vis_tlv)
        tau_vis_ref_tlv.Boost(-tau_tlv_boost)
        nu_tlv = build_tlorentz_vector(tau_nu)
        nu_ref_tlv = build_tlorentz_vector(tau_nu)
        nu_ref_tlv.Boost(-tau_tlv_boost)
        tau_vis_theta = tau_tlv.Angle(tau_vis_tlv.Vect())
        tau_vis_theta_star = tau_tlv.Angle(tau_vis_ref_tlv.Vect())
        tau_vis_theta_star_list = np.append(tau_vis_theta_star_list, [tau_vis_theta_star])
        tau_nu_theta = tau_tlv.Angle(nu_tlv.Vect())
        tau_nu_theta_star = tau_tlv.Angle(nu_ref_tlv.Vect())
        tau_nu_theta_star_list = np.append(tau_nu_theta_star_list, [tau_nu_theta_star])

        closest_tau, closest_tau_dr = nearest_branch_member_tlv(tau_vis_tlv, event.TauJets)

        # Fill overall histograms
        higgs_pt_hist.Fill(higgs.pt() / 1000.)
        tau_pt_hist.Fill(tau.pt() / 1000.)
        if vis_tau_final_lep_dr > high_dr:
            high_dr_tau_pt_hist.Fill(tau.pt() / 1000.)
        if tau.pt() / 1000. > high_pt:
            high_pt_dr_hist.Fill(vis_tau_final_lep_dr)
        if has_electron:
            tau_pt_ele_hist.Fill(tau.pt() / 1000.)
            vis_tau_final_lep_dr_ele_hist.Fill(vis_tau_final_lep_dr)
            tau_pt_vs_dr_ele_hist.Fill(tau.pt() / 1000., vis_tau_final_lep_dr)
        elif has_muon:
            tau_pt_mu_hist.Fill(tau.pt() / 1000.)
            vis_tau_final_lep_dr_mu_hist.Fill(vis_tau_final_lep_dr)
            tau_pt_vs_dr_mu_hist.Fill(tau.pt() / 1000., vis_tau_final_lep_dr)
        tau_eta_hist.Fill(tau.eta())
        tau_phi_hist.Fill(tau.phi())
        tau_vis_theta_hist.Fill(tau_vis_theta)
        tau_vis_theta_star_hist.Fill(tau_vis_theta_star)
        tau_nu_theta_hist.Fill(tau_nu_theta)
        tau_nu_theta_star_hist.Fill(tau_nu_theta_star)
        reco_tau_nearest_dr_hist.Fill(closest_tau_dr)
        truth_tau_nearest_dr_hist.Fill(closest_truth_tau_dr)
        vis_tau_final_lep_dr_hist.Fill(vis_tau_final_lep_dr)
        vis_tau_final_lep_deta_hist.Fill(vis_tau_final_lep_deta)
        vis_tau_final_lep_dphi_hist.Fill(vis_tau_final_lep_dphi)
        vis_tau_closest_b_dr_hist.Fill(vis_tau_closest_b_dr)
        tau_pt_vs_dr_hist.Fill(tau.pt() / 1000., vis_tau_final_lep_dr)
        vis_tau_final_lep_dr_eff_hist.Divide(vis_tau_final_lep_dr_hist)

        # Fill histograms for truth matched taus
        if closest_tau_dr < 0.1:
            tmtau_higgs_pt_hist.Fill(higgs.pt() / 1000.)
            higgs_pt_eff_hist.Fill(higgs.pt() / 1000.)
            tmtau_pt_hist.Fill(tau.pt() / 1000.)
            tau_pt_eff_hist.Fill(tau.pt() / 1000.)
            if vis_tau_final_lep_dr > high_dr:
                tmtau_high_dr_tau_pt_hist.Fill(tau.pt() / 1000.)
                high_dr_tau_pt_eff_hist.Fill(tau.pt() / 1000.)
            if tau.pt() / 1000. > high_pt:
                tmtau_high_pt_dr_hist.Fill(vis_tau_final_lep_dr)
                high_pt_dr_eff_hist.Fill(vis_tau_final_lep_dr)
            if has_electron:
                tmtau_pt_ele_hist.Fill(tau.pt() / 1000.)
                tau_pt_ele_eff_hist.Fill(tau.pt() / 1000.)
                tmtau_vis_tau_final_lep_dr_ele_hist.Fill(vis_tau_final_lep_dr)
                vis_tau_final_lep_dr_ele_eff_hist.Fill(vis_tau_final_lep_dr)
                tmtau_tau_pt_vs_dr_ele_hist.Fill(tau.pt() / 1000., vis_tau_final_lep_dr)
                tau_pt_vs_dr_ele_eff_hist.Fill(tau.pt() / 1000., vis_tau_final_lep_dr)
            elif has_muon:
                tmtau_pt_mu_hist.Fill(tau.pt() / 1000.)
                tau_pt_mu_eff_hist.Fill(tau.pt() / 1000.)
                tmtau_vis_tau_final_lep_dr_mu_hist.Fill(vis_tau_final_lep_dr)
                vis_tau_final_lep_dr_mu_eff_hist.Fill(vis_tau_final_lep_dr)
                tmtau_tau_pt_vs_dr_mu_hist.Fill(tau.pt() / 1000., vis_tau_final_lep_dr)
                tau_pt_vs_dr_mu_eff_hist.Fill(tau.pt() / 1000., vis_tau_final_lep_dr)
            tmtau_eta_hist.Fill(tau.eta())
            tau_eta_eff_hist.Fill(tau.eta())
            tmtau_phi_hist.Fill(tau.phi())
            tau_phi_eff_hist.Fill(tau.phi())
            tmtau_vis_theta_star_hist.Fill(tau_vis_theta_star)
            tau_vis_theta_star_eff_hist.Fill(tau_vis_theta_star)
            tmtau_nu_theta_star_hist.Fill(tau_nu_theta_star)
            tau_nu_theta_star_eff_hist.Fill(tau_nu_theta_star)
            tmtau_truth_tau_nearest_dr_hist.Fill(closest_truth_tau_dr)
            truth_tau_nearest_dr_eff_hist.Fill(closest_truth_tau_dr)
            tmtau_vis_tau_final_lep_dr_hist.Fill(vis_tau_final_lep_dr)
            vis_tau_final_lep_dr_eff_hist.Fill(vis_tau_final_lep_dr)
            tmtau_vis_tau_final_lep_deta_hist.Fill(vis_tau_final_lep_deta)
            vis_tau_final_lep_deta_eff_hist.Fill(vis_tau_final_lep_deta)
            tmtau_vis_tau_final_lep_dphi_hist.Fill(vis_tau_final_lep_dphi)
            vis_tau_final_lep_dphi_eff_hist.Fill(vis_tau_final_lep_dphi)
            tmtau_vis_tau_closest_b_dr_hist.Fill(vis_tau_closest_b_dr)
            vis_tau_closest_b_dr_eff_hist.Fill(vis_tau_closest_b_dr)
            tmtau_tau_pt_vs_dr_hist.Fill(tau.pt() / 1000., vis_tau_final_lep_dr)
            tau_pt_vs_dr_eff_hist.Fill(tau.pt() / 1000., vis_tau_final_lep_dr)
        # Fill histograms failing truth matching
        elif ftm_tau_pt_low < tau.pt() / 1000. < ftm_tau_pt_high:
            failed_match = 1
            ftm_tau_pt_hist.Fill(tau.pt() / 1000.)
            ftm_tau_eta_hist.Fill(tau.eta())
            ftm_tau_phi_hist.Fill(tau.phi())
            ftm_num_tau_hist.Fill(len(event.TauJets))
            ftm_tau_vis_theta_star_hist.Fill(tau_vis_theta_star)
            ftm_tau_nu_theta_star_hist.Fill(tau_nu_theta_star)

higgs_pt_eff_hist.Divide(higgs_pt_hist)
tau_pt_eff_hist.Divide(tau_pt_hist)
high_dr_tau_pt_eff_hist.Divide(high_dr_tau_pt_hist)
high_pt_dr_eff_hist.Divide(high_pt_dr_hist)
tau_pt_ele_eff_hist.Divide(tau_pt_ele_hist)
tau_pt_mu_eff_hist.Divide(tau_pt_mu_hist)
tau_eta_eff_hist.Divide(tau_eta_hist)
tau_phi_eff_hist.Divide(tau_phi_hist)
tau_vis_theta_star_eff_hist.Divide(tau_vis_theta_star_hist)
tau_nu_theta_star_eff_hist.Divide(tau_nu_theta_star_hist)
truth_tau_nearest_dr_eff_hist.Divide(truth_tau_nearest_dr_hist)
vis_tau_final_lep_dr_ele_eff_hist.Divide(vis_tau_final_lep_dr_ele_hist)
vis_tau_final_lep_dr_mu_eff_hist.Divide(vis_tau_final_lep_dr_mu_hist)
vis_tau_final_lep_deta_eff_hist.Divide(vis_tau_final_lep_deta_hist)
vis_tau_final_lep_dphi_eff_hist.Divide(vis_tau_final_lep_dphi_hist)
vis_tau_closest_b_dr_eff_hist.Divide(vis_tau_closest_b_dr_hist)
tau_pt_vs_dr_eff_hist.Divide(tau_pt_vs_dr_hist)
tau_pt_vs_dr_ele_eff_hist.Divide(tau_pt_vs_dr_ele_hist)
tau_pt_vs_dr_mu_eff_hist.Divide(tau_pt_vs_dr_mu_hist)

#tau_theta_stars = ROOT.TGraph(len(tau_nu_theta_star_list), tau_nu_theta_star_list, tau_vis_theta_star_list)
#tau_theta_stars = ROOT.TGraph()
#for i in range(len(tau_nu_theta_star_list)):
#    tau_theta_stars.SetPoint(i, tau_nu_theta_star_list[i], tau_vis_theta_star_list[i])
#tau_theta_stars.Write('tau_theta_stars')

root_out.Write()
root_out.Close()

print((datetime.datetime.now() - start_time).total_seconds() / 60, ' minutes' )
