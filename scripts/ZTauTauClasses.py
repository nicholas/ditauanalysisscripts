from rootEvent.eventClass import Event, OneToTwoDecay, Tau, DiTau
import rootEvent.helpers

class ZTauTauEvent(Event):
    '''
    An event with a di-tau object in the final state

    :param event: ROOT event
    '''
    def __init__(self, event):
        Event.__init__(self, event)

        z, taus = rootEvent.helpers.find_decay_mode(self.truth_particles, [15, -15], 23, 1)
        if z and taus:
            self.has_z_tautau = 1
            self.z_tautau = ZTauTau(z, taus[0], taus[1])
        else:
            self.has_z_tautau = 0

class ZTauTau(OneToTwoDecay, DiTau):
    '''
    A Z that decays to tautau. A subclass of the broader OneToTwoDecay class.

    :param z: Z TruthParticle
    :param tau: Tau TruthParticle
    :param anti_tau: Anti-Tau TruthParticle
    '''
    def __init__(self, z, tau, anti_tau):
        OneToTwoDecay.__init__(self, z, tau, anti_tau)
        self.z = z
        self.tau = Tau(tau)
        self.anti_tau = Tau(anti_tau)
        DiTau.__init__(self, self.tau, self.anti_tau)
        
